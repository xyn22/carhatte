<?php
/* @var $installer DdD_Api_Model_Resource_Setup */
$installer = $this;
//$installer->removeAttribute(Mage_Catalog_Model_Category::ENTITY, 'ddd_category_id');
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'ddd_category_id', array(
    'group'             => 'General Information',
    'type'              => 'varchar',
    'backend'           => '',
    'label'             => 'DdD Category ID',
    'input'             => 'text',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
    'visible_on_front'  => false,
    'unique'            => true,
));