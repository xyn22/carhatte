<?php

$installer = $this;
$installer->startSetup();

$attributes[] = array('code' => 'ean', 'group' => 'General', 'label' => 'EAN13', 'note' => 'European Article Number', 'input' => 'text', 'visible' => true, 'type' => 'text');

$setup = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup();

if (true) {
    foreach($attributes as $oneAttribute)
    {
        if ( ($attr = $setup->getAttributeId(Mage_Catalog_Model_Product::ENTITY, $oneAttribute['code'])) === false)
        {
            $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, $oneAttribute['code'], array(
                'group' => $oneAttribute['group'],
                'sort_order' => $oneAttribute['sortorder'],
                'type' =>  isset($oneAttribute['type']) ? $oneAttribute['type'] : 'varchar',
                'backend' => ( $oneAttribute['input'] == 'multiselect' ? 'eav/entity_attribute_backend_array' : '' ),
                'frontend' => '',
                'label' => $oneAttribute['label'],
                'note' => $oneAttribute['note'],
                'input' => $oneAttribute['input'],
                'class' => '',
                'source' => '',
                'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                'required' => false,
                'user_defined' => true,
                'default' => '',
                'unique' => false,
                'used_for_promo_rules' => false,
                
                'visible' => $oneAttribute['visible'],
                'visible_on_front' => $oneAttribute['visibleonfront'],
                'visible_in_advanced_search' => isset($oneAttribute['visibleinadvancedsearch'])?$oneAttribute['visibleinadvancedsearch']:false,
                
                'is_searchable' => $oneAttribute['searchable']?$oneAttribute['searchable']:false,
                'is_configurable' => $oneAttribute['configurable']?$oneAttribute['configurable']:false,
                'is_filterable' => $oneAttribute['filterable']?$oneAttribute['filterable']:false,
                ));
        }
    }
}

$installer->endSetup();


