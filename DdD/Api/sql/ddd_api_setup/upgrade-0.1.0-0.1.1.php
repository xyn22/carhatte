<?php
/* @var $installer DdD_Api_Model_Resource_Setup */
$installer = $this;

$colorAttribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'color');
$colorAttribute->setData('source_model', 'eav/entity_attribute_source_table');
$colorAttribute->save();
