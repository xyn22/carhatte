<?php
/* @var $installer DdD_Api_Model_Resource_Setup */
$installer = $this;

$defaultAttributeOptions = array(
    'group' => 'General',
    'input' => 'text',
    'type' => 'text',
    'frontend' => '',
    'class' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => false,
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'used_for_promo_rules' => false,
    'visible' => true,
    'visible_in_advanced_search' => false,
    'is_searchable' => false,
    'is_configurable' => false,
    'is_filterable' => false
);
$attributes = array(
    'ean' => array(
        'group' => 'General',
        'label' => 'EAN13',
        'note' => 'European Article Number',
        'unique' => true
    ),
    'edb_number' => array(
        'group' => 'DdD Params',
        'label' => 'EDB Number',
        'note' => 'The key in DdD to match wich article was sold',
        'unique' => true
    ),
    'item_group' => array(
        'group' => 'DdD Params',
        'label' => 'Item Group',
        'note' => 'The DdD item group number that the article belongs to'
    ),
    'supplier' => array(
        'group' => 'DdD Params',
        'label' => 'Supplier',
        'note' => 'The DdD supplier of the article'
    ),
    'supplier_id' => array(
        'group' => 'DdD Params',
        'label' => 'Supplier ID',
        'note' => 'The DdD supplier id of the article'
    )
);
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
foreach ($attributes as $attributeCode => $attributeOptions) {
    $attributeOptions += $defaultAttributeOptions;
    if ($installer->getAttributeId($entityTypeId, $attributeCode)) {
        foreach ($attributeOptions as $field => $value) {
            $installer->updateAttribute($entityTypeId, $attributeCode, $field, $value);
        }
    } else {
        $installer->addAttribute($entityTypeId, $attributeCode, $attributeOptions);
    }
}
$installer->endSetup();