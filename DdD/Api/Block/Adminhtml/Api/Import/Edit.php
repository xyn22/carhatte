<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api_Block_Adminhtml_Api_Import_Edit
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Api_Block_Adminhtml_Api_Import_Edit extends 
    Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->removeButton('back')
            ->removeButton('reset')
            ->_updateButton('save', 'label', $this->__('Run Import'))
            ->_updateButton('save', 'id', 'run_import')
            ->_updateButton('save', 'onclick', 'DddService.start()');
    }
    
    protected function _construct()
    {
        parent::_construct();
        
        $this->_objectId   = 'ddd_api_import_id';
        $this->_blockGroup = 'ddd_api';
        $this->_controller = 'adminhtml_api_import';
    }
    
    public function getHeaderText()
    {
        return Mage::helper('ddd_api')->__('DdD Import');
    }
}