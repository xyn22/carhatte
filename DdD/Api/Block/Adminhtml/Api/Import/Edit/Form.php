<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api_Block_Adminhtml_Api_Import_Edit_Form
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Api_Block_Adminhtml_Api_Import_Edit_Form
    extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $helper = Mage::helper('ddd_api');
        $form = new Varien_Data_Form(array(
            'id'      => 'edit_form',
            'action'  => $this->getUrl('*/*/index'),
            'method'  => 'post',
            'enctype' => 'multipart/form-data'
        ));
        $fieldset = $form->addFieldset('base_fieldset', array('legend' => $helper->__('Process Settings')));
        $fieldset->addField('entity', 'select', array(
            'name'     => 'entity',
            'title'    => $helper->__('Entity Type'),
            'label'    => $helper->__('Entity Type'),
            'required' => true,
            'values'   => Mage::getModel('ddd_api/source_import_entity')->toOptionArray()
        ));
        $fieldset->addField('client_id', 'select', array(
            'name'     => 'client_id',
            'title'    => $helper->__('Client ID'),
            'label'    => $helper->__('Client ID'),
            'required' => true,
            'values'   => Mage::getModel('ddd_api/source_import_clientid')->toOptionArray()
        ));
        $fieldset->addField('method', 'select', array(
            'name'     => 'method',
            'title'    => $helper->__('Import Method'),
            'label'    => $helper->__('Import Method'),
            'required' => true,
            'values'   => Mage::getModel('ddd_api/source_import_method')->toOptionArray()
        ));
        $fieldset->addField('start', 'text', array(
            'name'     => 'start',
            'title'    => $helper->__('Start From'),
            'label'    => $helper->__('Start From'),
            'required' => true,
            'value'    => 0,
        ));
        $fieldset->addField('limit', 'text', array(
            'name'     => 'limit',
            'title'    => $helper->__('Limit'),
            'label'    => $helper->__('Limit'),
            'required' => true,
            'value'    => 0,
        ));
        $fieldset->addField('new_only', 'checkbox', array(
            'name'     => 'new_only',
            'title'    => $helper->__('New Products Only'),
            'label'    => $helper->__('New Products Only'),
            'required' => false,
            'value'    => 1,
        ));
        $fieldset->addField('step', 'text', array(
            'name'     => 'step',
            'title'    => $helper->__('Batch Size'),
            'label'    => $helper->__('Batch Size'),
            'required' => true,
            'value'    => 200,
        ));
        
        $form->setUseContainer(true);
        $this->setForm($form);
    
        return parent::_prepareForm();
    }
}