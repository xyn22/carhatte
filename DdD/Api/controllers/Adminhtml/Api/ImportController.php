<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Api_Adminhtml_Api_ImportController extends Mage_Adminhtml_Controller_Action
{
    protected function _construct()
    {
        $this->setUsedModuleName('DdD_Api');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('system/convert/ddd_api_import');
    }

    protected function _initAction()
    {
        $this->_title($this->__('DdD Import'))
            ->loadLayout()
            ->_setActiveMenu('system/ddd_api');
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->_title($this->__('DdD Import'))
            ->_addBreadcrumb($this->__('DdD Import'), $this->__('DdD Import'));
        $this->renderLayout();
    }

    public function processAction()
    {
        $data = $this->getRequest()->getParams();
        $data += array('client_id' => Mage::helper('ddd_api')->getClientIds());
        if ($data) {
            $this->loadLayout(false);
            try {
                $importModel = Mage::getModel('ddd_api/import')
                    ->setData($data);
                $status = $importModel->importSource();
            } catch (Exception $e) {
                Mage::logException($e);
                $error = $e->getMessage();
                $status = compact('error');
            }
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(json_encode($status));
    }
}