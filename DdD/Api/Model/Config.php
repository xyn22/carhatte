<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api_Config
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Api_Model_Config extends Varien_Object
{
    const XPATH_GENERAL_API_KEY = 'ddd_api_settings/general/key';
    const XPATH_GENERAL_API_ENABLED = 'ddd_api_settings/general/enabled';
    const XPATH_GENERAL_STOCK_IMPORT_ENABLED = 'ddd_api_settings/general/stock_import_enabled';
    const XPATH_GENERAL_SALE_SERVICE_ENABLED = 'ddd_api_settings/general/sale_service_enabled';
    const XPATH_GENERAL_CLIENT_IDS = 'ddd_api_settings/general/clientids';

    const XPATH_CRON_ENABLED = 'ddd_api_settings/cron/enabled';
    const XPATH_CRON_NEW_PRODUCTS_ONLY = 'ddd_api_settings/cron/new_products_only';

    public function getApiKey()
    {
        return Mage::getStoreConfig(self::XPATH_GENERAL_API_KEY);
    }

    public function isApiEnabled()
    {
        return Mage::getStoreConfig(self::XPATH_GENERAL_API_ENABLED);
    }

    public function isStockImportEnabled()
    {
        return $this->isApiEnabled() && Mage::getStoreConfig(self::XPATH_GENERAL_STOCK_IMPORT_ENABLED);
    }

    public function isSaleServiceEnabled()
    {
        return $this->isApiEnabled() && Mage::getStoreConfig(self::XPATH_GENERAL_SALE_SERVICE_ENABLED);
    }

    public function isCronEnabled()
    {
        return Mage::getStoreConfig(self::XPATH_CRON_ENABLED);
    }

    public function newProductsOnly()
    {
        return Mage::getStoreConfig(self::XPATH_CRON_NEW_PRODUCTS_ONLY);
    }

    public function getClientIds()
    {
        return Mage::getStoreConfig(self::XPATH_GENERAL_CLIENT_IDS);
    }

    public function getFirstClientId()
    {
        list($clientId,) = explode(',', $this->getClientIds());
        return $clientId;
    }
}