<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api_Model_Import_Product
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Api_Model_Import_Product extends DdD_Api_Model_Import_Abstract
{

    const STATUS_KEY = 'ddd_api_product_import';

    const MSG_SKU_NOT_EXISTS = '[EAN: {:ean}] Product SKU does not exists';
    const MSG_ATTR_NOT_EXISTS = '[EAN: {:ean}] The attribute "{:attribute}" for EAN "{:ean}" does not exists';
    const MSG_UNKNOW_PRODUCT_TYPE = '[EAN: {:ean}] Product type for SKU "{:sku}" has not detected';
    const MSG_CATEGORY_NOT_EXISTS = '[EAN: {:ean}] Category "{:category_name}" with DdD ID "{:item_group}" not associated';
    const MSG_SKIP_ITEM_EXISTS = '[EAN: {:ean}] Skip item because exists';

    protected $_allData;

    protected $_currentData;

    protected $_productCache;

    protected $_attributeCache;

    protected $_categoryCache;

    protected $_importedProducts = array();

    protected $_importedEans;

    protected function _importData()
    {
        $productData = $this->_getProductData();
        foreach($productData as $data) {
            if ($this->isImportCompleted()) {
                break;
            }
            $this->count++;
            $this->_prepareProductData($data);
            Mage::log('Count: ' . $this->count . ', Total: ' . $this->total);
            $this->_importProduct();
        }
        if ($this->isImportCompleted()) {
            $this->_importCompleted();
            Mage::log('================= Import completed! =================');
        }
        return $this;
    }

    protected function _getProductData()
    {
        Mage::log("Process data: " . json_encode($this->getData()));
        if (null === $this->_allData) {
            $this->_allData = $this->getStockHelper($this->clientId)
                ->getProductResult()
                ->getData();
            $this->total = count($this->_allData);
            if ($this->method == DdD_Api_Model_Import::METHOD_IMPORT_PARTIAL) {
                $this->limit  = $this->limit ? $this->limit : null;
                $this->_allData = array_slice($this->_allData, $this->start, $this->limit);
                $this->total = count($this->_allData);
            }
        }
        return array_slice($this->_allData, $this->stepStart, $this->step);
    }

    protected function _prepareProductData($data)
    {
        if (is_string($data)) {
            $data = json_decode($data, true);
        }
        $this->_currentData = $this->_remapData($data);
        return $this;
    }

    protected function _remapData(array $data)
    {
        $mapper = array(
            'ItemGroupName' => 'category_name',
            'ItemGroup' => 'item_group',
            'SalesPriceEach' => 'price',
            'CostpriceEach' => 'cost',
            'Ean' => 'ean',
            'Qty' => 'qty',
            'Kparam1' => 'sku',
            'Kparam2' => 'name,description,short_description',
            'Kparam3' => 'kparam3',
            'Kparam4' => 'kparam4',
            'Kparam5' => 'kparam5',
            'Vparam1' => 'color',
            'Vparam2' => '',
            'Vparam3' => 'size',
            'Vparam4' => '',
            'Vparam5' => '',
            'KatalogNumber' => '',
            'EdbNumber' => 'edb_number',
            'SupplierName' => 'supplier',
            'Supplier' => 'supplier_id',
        );
        foreach ($mapper as $from => $to) {
            $data[$from] = trim($data[$from]);
            if ($to === '') {
                unset($data[$from]);
                continue;
            }
            if (isset($data[$from])) {
                foreach (explode(',', $to) as $field) {
                    $data[$field] = $data[$from];
                }
                unset($data[$from]);
            }
        }
        return $data;
    }

    protected function _isImportNeeded($data = null)
    {
        if (null === $data) {
            $data = $this->_currentData;
        }
        if ($this->newOnly && $this->_eanExists($data['ean'])) {
            return false;
        }
        return true;
    }

    protected function _eanExists($ean)
    {
        if (null === $this->_importedEans) {
            $this->_importedEans = $this->_getImportedEans();
        }
        if (in_array($ean, $this->_importedEans)) {
            return true;
        }
        return false;
    }

    protected function _getImportedEans()
    {
        $resource = Mage::getSingleton('core/resource');
        $attributeTable = $resource->getTableName('eav_attribute');
        $productValueTable = $resource->getTableName('catalog_product_entity_text');
        $connection = $resource->getConnection('core_read');
        $query = "SELECT p.value FROM {$productValueTable} as p
LEFT JOIN {$attributeTable} as a ON a.attribute_id=p.attribute_id
WHERE a.attribute_code='ean' AND p.value IS NOT NULL";
        return $connection->fetchCol($query);
    }

    protected function _importProduct()
    {
        if (!$this->_isImportNeeded()) {
            return;
        }
        if ($this->_validateData()) {
            try {
                $this->_saveProduct();
            } catch (Exception $e) {
                Mage::logException($e);
                $this->addErrorMessage($e->getMessage(), $this->_currentData);
            }
        }
    }

    protected function _validateData()
    {
        $this->_currentData['sku'] = preg_replace('%\s%', '', $this->_currentData['sku']);
        if (empty($this->_currentData['sku'])) {
            $this->addErrorMessage(static::MSG_SKU_NOT_EXISTS, $this->_currentData);
            return false;
        }
        $requiredAttributes = array('');
        if (empty($this->_currentData['name'])) {
            $this->addWarnMessage(self::MSG_ATTR_NOT_EXISTS, array('attribute' => 'name') + $this->_currentData);
        }
        return true;
    }

    protected function _saveProduct()
    {
        Mage::log('Import product ' . $this->_count);
        $productData = $this->_currentData;
        $isSubProduct = $this->_isSubProduct();
        if ($isSubProduct) {
            $configurableProduct = $this->_getConfigurableProduct();
        }
        $sku = $isSubProduct ? $productData['ean'] : $productData['sku'];
        $defaults = array(
            'type_id' => 'simple',
            'visibility' => $isSubProduct ? 1 : 4,
            'status' => 1,
            'attribute_set_id' => 9,
            'tax_class_id' => 1,
            'website_ids' => array(1),
            'weight' => 0,
        );
        $productId = Mage::getModel('catalog/product')->getIdBySku($sku);
        /* @var $product Mage_Catalog_Model_Product */
        $product = Mage::getModel('catalog/product');
        if ($productId) {
            $productData = array_intersect_key($productData, array_flip(array(
                'price', 'edb_number', 'item_group', 'supplier', 'supplier_id')));
            $product->load($productId);
        } else {
            $productData += $defaults;
            $productData = $this->_attributeLabelsToValues($productData);
        }
        Mage::log("Product data ({$sku}): " . json_encode($productData));
        $product->addData($productData);
        $product->setData('sku', $sku);
        if (!$isSubProduct && !$productId) {
            $this->_addProductToCategory($product);
        }
        if($isSubProduct && !$productId) {
            $product->setStockData(array(
                'is_in_stock' => 0,
                'qty' => 0,
                'use_config_manage_stock' => 1
            ));
        }
        $product->save();
        
        if (isset($configurableProduct) && $configurableProduct->getId()) {
            $this->_assignProductToConfigurable($product, $configurableProduct);
        }
    }

    protected function _isSubProduct()
    {
        if (!empty($this->_currentData['color']) || !empty($this->_currentData['size'])) {
            return true;
        }
        return false;
    }

    protected function _addProductToCategory($product)
    {
        $dddCategoryId = $this->_currentData['item_group'];
        if (!$dddCategoryId) {
            return $this;
        }
        $categoryId = $this->_getCategoryIdByDdDCategoryId($dddCategoryId);
        if (!$categoryId) {
            return $this->addWarnMessage(static::MSG_CATEGORY_NOT_EXISTS, $this->_currentData);
        }
        $product->setCategoryIds(array($categoryId));
        return $this;
    }

    protected function _getCategoryIdByDdDCategoryId($dddCategoryId)
    {
        if (isset($this->_categoryCache[$dddCategoryId])) {
            return $this->_categoryCache[$dddCategoryId];
        }
        $category = Mage::getModel('catalog/category')->loadByAttribute('ddd_category_id', $dddCategoryId);
        if ($category) {
            $this->_categoryCache[$dddCategoryId] = $category->getId();
            return $this->_categoryCache[$dddCategoryId];
        }
    }

    protected function _getConfigurableProduct()
    {
        $sku = $this->_currentData['sku'];
        $product = $this->_getProduct($sku);
        if (!$product->getId()) {
            $defaults = array(
                'type_id' => 'configurable',
                'visibility' => 4,
                'status' => 2,
                'attribute_set_id' => 9,
                'news_from_date' => date('Y-m-d H:i:s', time()),
                'tax_class_id' => 1,
                'website_ids' => array(1),
            );
            $emptyDefauls = array('ean' => null, 'edb_number' => null);
            $productData = $emptyDefauls + $this->_currentData;
            $productData += $defaults;
            $product->addData($productData);
            $attributeCodes = $this->_getSuperAttributeCodes();
            $product->setConfigurableProductData(array());
            $product->setConfigurableAttributesData($this->_getConfigurableAttributesData($attributeCodes));
            $product->setCanSaveConfigurableAttributes(true);
            $stockData = array(
                'use_config_manage_stock' => 1,
                'use_config_enable_qty_increments' => 1,
                'is_in_stock' => 1
            );
            $product->setStockData($stockData);
            $this->_addProductToCategory($product);
            $product->save();
        }
        return $product;
    }

    protected function _attributeLabelsToValues($data)
    {
        $attributeCodes = $this->_getSuperAttributeCodes($data);
        foreach ($attributeCodes as $attributeCode) {
            if (empty($data[$attributeCode])) {
                continue;
            }
            $optionLabel = trim($data[$attributeCode]);
            $data[$attributeCode] = $this->_getAttributeOptionValue($attributeCode, $optionLabel);
        }
        return $data;
    }

    protected function _getAttributeOptionValue($attributeCode, $optionLabel)
    {
        try {
            $attribute = $this->_getAttribute($attributeCode);
            $options = (array) $attribute->getSource()->getAllOptions(false);
            foreach ($options as $option) {
                if (strtolower($option['label']) == strtolower($optionLabel)) {
                    return $option['value'];
                }
            }
            $value = array('option' => array($optionLabel));
            $attribute->setData('option', compact('value'));
            $attribute->save();
            unset($this->_attributeCache[$attributeCode]);
            return $this->_getAttributeOptionValue($attributeCode, $optionLabel);
        } catch (Exception $e) {
            Mage::logException($e);
            $this->addErrorMessage($e->getMessage(), $this->_currentData);
        }
    }

    protected function _assignProductToConfigurable($product, $configurableProduct)
    {
        $childProductIds = $configurableProduct->getTypeInstance()->getUsedProductIds();
        $childProductIds[] = $product->getId();
        $childProductIds = array_unique($childProductIds);
        $productTypeConfigurable = Mage::getResourceModel('catalog/product_type_configurable');
        $productTypeConfigurable->saveProducts($configurableProduct, $childProductIds);
        return $this;
    }

    protected function _getConfigurableAttributesData(array $attributeCodes)
    {
        $attributesData = array();
        foreach ($attributeCodes as $code) {
            $attributesData[]= $this->_getAttribute($code)->getData();
        }
        return $attributesData;
    }

    protected function _getSuperAttributeCodes()
    {
        $superAttributeCodes = array();
        foreach(array('size', 'color') as $code) {
            if (!empty($this->_currentData[$code])) {
                $superAttributeCodes[] = $code;
            }
        }
        return $superAttributeCodes;
    }

    protected function _getProduct($sku) {
        if (!isset($this->_productCache[$sku])) {
            $product = Mage::getModel('catalog/product');
            $productId = Mage::getModel('catalog/product')->getIdBySku($sku);
            if ($productId) {
                $product->load($productId);
            }
            $this->_productCache = array();
            $this->_productCache[$sku] = $product;
        }
        return $this->_productCache[$sku];
    }

    protected function _getAttribute($attributeCode) {
        if (!isset($this->_attributeCache[$attributeCode])) {
            $this->_attributeCache[$attributeCode] = Mage::getModel('eav/entity_attribute')
                ->loadByCode('catalog_product', $attributeCode);
        }
        return $this->_attributeCache[$attributeCode];
    }
}