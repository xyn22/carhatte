<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Api_Model_Import_Result_Abstract implements Iterator, Countable
{
    
    protected $_data = array();

    public function __construct($data = array())
    {
        if (is_string($data)) {
            $this->setDataJson($data);
        } else {
            $this->setData($data);
        }
    }

    public function setJsonData($jsonData)
    {
        foreach (explode('},{', trim($jsonData, '[]{}')) as $dataLineString) {
            $dataLineString = '{' . $dataLineString . '}';
            $this->_data[] = $dataLineString;
        }
        return $this;
    }

    public function setData($data)
    {
        if (is_string($data)) {
            return $this->setJsonData($data);
        }
        $this->_data = (array) $data;
        return $this;
    }

    public function getData()
    {
        return $this->_data;
    }

    public function current()
    {
        return current($this->_data);
    }

    public function next()
    {
        return next($this->_data);
    }

    public function key()
    {
        return key($this->_data);
    }

    public function valid()
    {
        $key = $this->key();
        return isset($this->_data[$key]);
    }

    public function rewind()
    {
        reset($this->_data);
    }

    public function count()
    {
        return count($this->_data);
    }
}
