<?php

/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api_Model_Service_Abstract
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */
abstract class DdD_Api_Model_Import_Abstract extends Varien_Object
{

    const MSG_ERROR = 'error';

    const MSG_WARN = 'warn';

    const MSG_INFO = 'info';

    protected $_messages = array();

    protected $_runIndexer = false;

    public function setRunIndexer($runIndexer)
    {
        $this->_runIndexer = $runIndexer;
        return $this;
    }

    public function getMessages()
    {
        return $this->_messages;
    }

    public function addErrorMessage($message, array $data = array())
    {
        return $this->_addMessage(static::MSG_ERROR, $message, $data);
    }

    public function addInfoMessage($message, array $data = array())
    {
        return $this->_addMessage(static::MSG_INFO, $message, $data);
    }

    public function addWarnMessage($message, array $data = array())
    {
        return $this->_addMessage(static::MSG_WARN, $message, $data);
    }

    public function isImportCompleted()
    {
        if (!$this->total) {
            return true;
        }
        if ($this->count && $this->count >= $this->total) {
            return true;
        }
        return false;
    }

    public function getStatus()
    {
        $total = $this->total;
        $count = $this->count;
        $messages = $this->getMessages();
        if ($this->isImportCompleted()) {
            $completed = true;
        }
        return compact('total', 'count', 'messages', 'completed');
    }

    /**
     * @return DdD_Api_Helper_Stock
     */
    public function getStockHelper($clientId, $useCache = true)
    {
        $helper = Mage::helper('ddd_api/stock')
            ->setUseCache($useCache)
            ->setClientId($clientId);
        return $helper;
    }
    
    public function importData()
    {
        $this->_beforeImport();
        $counter = 1;
        do {
            try {
                $this->_importData();
            } catch (Exception $e) {
                if ($counter <= 5) {
                    sleep(pow(2, $counter));
                    $counter++;
                    Mage::logException($e);
                }
            }
        } while ($counter <= 5 && $counter > 1);
        $this->_afterImport();
        return $this;
    }

    abstract protected function _importData();

    protected function _beforeImport()
    {
        if ((int) $this->count <= 0) {
            $this->_clearCache();
        }
        if ($this->_runIndexer) {
            $this->_updateIndexerMode(Mage_Index_Model_Process::MODE_MANUAL);
        }
    }
    
    protected function _afterImport()
    {
        if ($this->_runIndexer) {
            $this->_runIndexer();
            $this->_updateIndexerMode(Mage_Index_Model_Process::MODE_REAL_TIME);
        }
    }

    protected function _addMessage($type, $message, array $data = array())
    {
        foreach ($data as $key => $value) {
            $message = str_replace("{:{$key}}", $value, $message);
        }
        extract($data, EXTR_SKIP);
        $this->_messages[] = compact('sku', 'ean', 'name', 'message', 'type');
        return $this;
    }

    protected function _runIndexer()
    {
        for ($i = 1; $i <= 9; $i ++) {
            $process = Mage::getModel('index/process')->load($i);
            if ($process->getStatus() == Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX) {
                $process->reindexAll();
            }
        }
        $this->addInfoMessage('Reindex data completed');
    }

    protected function _updateIndexerMode($mode)
    {
        for ($i = 1; $i <= 9; $i ++) {
            $process = Mage::getModel('index/process')->load($i);
            $process->setMode($mode)->save();
            $process->save();
        }
        return $this;
    }

    protected function _importCompleted()
    {
        $this->_clearCache();
        $this->addInfoMessage('Import completed');
        return $this;
    }
    
    protected function _clearCache()
    {
        Mage::helper('ddd_api/stock')->clearCache();
        return $this;
    }
}