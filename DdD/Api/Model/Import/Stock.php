<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api_Model_Import_Stock
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Api_Model_Import_Stock extends DdD_Api_Model_Import_Abstract
{
    protected $_allEans;

    protected $_stockData;
    
    protected $_isNeedProcessing = false;

    protected function _beforeImport()
    {
        parent::_beforeImport();
        $this->_prepareAllEans();
        $this->_prepareStockData();
    }

    protected function _importData()
    {
        if (!count($this->_stockData)) {
            return;
        }
        foreach ($this->_getProductCollection() as $product) {
            $this->count++;
            Mage::log('Count: ' . $this->count . ', Total: ' . $this->total);
            $product = Mage::getModel('catalog/product')->load($product->getId());
            $this->updateStockData($product);
            if ($this->_isNeedProcessing) {
                $this->updateStockItem($product);
            }
        }
        if ($this->isImportCompleted()) {
            $this->_importCompleted();
            Mage::log('================= Stock Import completed! =================');
        }
    }

    public function updateStockData($product)
    {
        $allQty = 0;
        $negativeValue = 0;
        $isNeedProcessing = false;
        foreach ($this->_getClientIds() as $clientId) {
            $warehouseQty = $this->_getQtyByEan($product->getEan(), $clientId);
            if ($negativeValue < 0) {
                $warehouseQty += $negativeValue;
            }
            if ($warehouseQty < 0) {
                $warehouseQty = 0;
                $negativeValue = $warehouseQty;
            }
            $warehouseAttributeName = $this->_getWarehouseAttributeName($clientId);
            $oldWarehouseQty = $product->getData($warehouseAttributeName);
            if ($oldWarehouseQty != $warehouseQty) {
                $isNeedProcessing = true;
            }
            $product->setData($warehouseAttributeName, $warehouseQty);
            $allQty += $warehouseQty;
            Mage::log("Warehouse Qty [{$product->getEan()}]: " . json_encode(array('warehouse' => $warehouseAttributeName, 'qty' => $warehouseQty)));
        }
        if (!$allQty && $isNeedProcessing) {
            $product->setStockData(array(
                'qty' => $allQty,
                'is_in_stock' => $allQty ? 1 : 0,
                'use_config_manage_stock' => 1
            ));
        }
        if ($isNeedProcessing) {
            $product->setAllQty($allQty);
            $product->save();
            $this->_isNeedProcessing = true;
        } else {
            $this->_isNeedProcessing = false;
            Mage::log("Update not needed for SKU {$product->getSku()}");
        }
        
        return $this;
    }

    public function updateStockItem($product)
    {
        $stockData = (array) $product->getStockData();
        $allQty = $product->getAllQty();
        $productId = $product->getId();
        $newStockData = array(
            'qty' => $allQty,
            'is_in_stock' => $allQty ? 1 : 0,
            'use_config_manage_stock' => 1
        );
        $stockData = array('stock_id' => 1) + $newStockData + $stockData;
        Mage::log("Stock data [{$product->getEan()}]: " . json_encode($stockData));
        
        /*$stockItem = Mage::getModel('cataloginventory/stock_item');
        $stockItem->assignProduct($product);
        $stockItem->addData($stockData);
        $stockItem->save();*/
        $this->_rawUpdateStockItem($stockData, $product);
        
        return $this;
    }
    
    protected function _rawUpdateStockItem($data, $product)
    {
        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_read');
        
        $sql = "UPDATE " . $resource->getTableName('cataloginventory_stock_item') . " csi,
            " . $resource->getTableName('cataloginventory_stock_status') . " css
            SET
            csi.qty = ?,
            csi.is_in_stock = ?,
            css.qty = ?,
            css.stock_status = ?
            WHERE
            csi.product_id = ?
            AND csi.product_id = css.product_id";
        try {
            $connection->query($sql, array($data['qty'], $data['is_in_stock'], $data['qty'], $data['is_in_stock'], $product->getId()));
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::log("ERROR: Updating qty {$data['qty']} for product SKU {$product->getSku()}");
        }
    }

    protected function _getProductCollection()
    {
        /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToFilter('type_id', 'simple')
            ->setOrder('created_at', Varien_Db_Select::SQL_DESC);
        $collection->getSelect()->limit($this->getStep(), $this->getStepStart());
        $this->total = $collection->getSize();
        Mage::log("Start: {$this->getStepStart()}, Limit: {$this->getStep()}, Total: {$this->total}");
        return $collection;
    }

    protected function _getQtyByEan($ean, $clientId)
    {
        if (!isset($this->_stockData[$clientId]) && !isset($this->_stockData[$clientId][$ean])) {
            return 0;
        }
        return $this->_stockData[$clientId][$ean];
    }

    protected function _getWarehouseAttributeName($clientId)
    {
        return Mage::helper('ddd_warehouse')->getWarehouseById($clientId);
    }

    protected function _prepareStockData()
    {
        if (null === $this->_stockData) {
            foreach ($this->_getClientIds() as $clientId) {
                $allData = $this->getStockHelper($clientId)
                    ->getStockResult(array('eans' => $this->_allEans));
                foreach ($allData as $data) {
                    $data = json_decode($data);
                    $this->_stockData[$clientId][$data->Key] = $data->Value;
                }
            }
        }
        return $this;
    }

    protected function _prepareAllEans()
    {
        if (null === $this->_allEans) {
            $this->_allEans = $this->_getAllStoredEans();
        }
        return $this->_allEans;
    }

    protected function _getAllStoredEans()
    {
        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_read');
        
        $sql = "SELECT v.value as ean
FROM {$resource->getTableName('catalog_product_entity_text')} as v
INNER JOIN  {$resource->getTableName('eav_attribute')} as a ON a.attribute_id=v.attribute_id AND attribute_code='ean'
WHERE v.store_id=0 AND v.value IS NOT NULL;";
        return $connection->fetchCol($sql);
    }

    protected function _getClientIds()
    {
        return (array) explode(',', $this->clientId);
    }
    
    protected function _afterImport()
    {
        try {
            $indexer = Mage::getModel('index/indexer')->getProcessByCode('cataloginventory_stock');
            $indexer->reindexEverything();
            Mage::app()->getCacheInstance()->flush();
            Mage::app()->cleanCache();
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
}