<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api_Model_Api_Sales_Request_Item
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Api_Model_Api_Sales_Request_Item extends DdD_Api_Model_Api_Sales_Request_Abstract
{
    public function invoiceItemToRequest(Mage_Sales_Model_Order_Invoice_Item $invoiceItem)
    {
        $this->unsetData();
        $this->_prepareData($this->_getInvoiceItemDataMapping($invoiceItem));
        return $this;
    }
    
    public function creditmemoItemToRequest(Mage_Sales_Model_Order_Creditmemo_Item $creditmemoItem)
    {
        $this->unsetData();
        $this->_prepareData($this->_getCreditmemoItemDataMapping($creditmemoItem));
        return $this;
    }
    
    public function orderItemToRequest(Mage_Sales_Model_Order_Item $orderItem)
    {
        $this->unsetData();
        $this->_prepareData($this->_getOrderItemDataMapping($orderItem));
        return $this;
    }
    
    protected function _getInvoiceItemDataMapping(Mage_Sales_Model_Order_Invoice_Item $invoiceItem)
    {
        $product = $this->_getProductBySku($invoiceItem->getSku());
        return array(
            'terminal' => 99,
            'type' => 'Sale',
            'e_db_number' => $product->getData('edb_number'),
            'supplier' => $product->getData('supplier'),
            'item_group' => $product->getData('item_group'),
            'qty' => (int) $invoiceItem->getQty(),
            'line_amount' => (float) $invoiceItem->getRowTotalInclTax(),
            'discount_amount' => (float) ($invoiceItem->getDiscountAmount() * (-1)),
            'time' => time()
        );
    }

    protected function _getCreditmemoItemDataMapping(Mage_Sales_Model_Order_Creditmemo_Item $creditmemoItem)
    {
        $product = $this->_getProductBySku($creditmemoItem->getSku());
        return array(
            'terminal' => 99,
            'type' => 'Sale',
            'e_db_number' => $product->getData('edb_number'),
            'supplier' => $product->getData('supplier'),
            'item_group' => $product->getData('item_group'),
            'qty' => (int) ($creditmemoItem->getQty() * (-1)),
            'line_amount' => (float) $creditmemoItem->getRowTotalInclTax(),
            'discount_amount' => (float) ($creditmemoItem->getDiscountAmount() * (-1)),
            'time' => time()
        );
    }
    
    protected function _getOrderItemDataMapping(Mage_Sales_Model_Order_Item $orderItem)
    {
        $product = $this->_getProductBySku($orderItem->getSku());
        $qty = $orderItem->getQtyOrdered();
        if ($orderItem->getStatusId() == Mage_Sales_Model_Order_Item::STATUS_CANCELED) {
            $qty = $orderItem->getQtyOrdered() * (-1); 
        }
        return array(
            'terminal' => 99,
            'type' => 'Sale',
            'e_db_number' => $product->getData('edb_number'),
            'supplier' => $product->getData('supplier'),
            'item_group' => $product->getData('item_group'),
            'qty' => $qty,
            'line_amount' => (float) $orderItem->getRowTotalInclTax(),
            'discount_amount' => (float) ($orderItem->getDiscountAmount() * (-1)),
            'time' => time()
        );
    }
    
    protected function _getProductBySku($sku)
    {
        return Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
    }
}