<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api_Model_Sales_Request
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Api_Model_Api_Sales_Request extends DdD_Api_Model_Api_Sales_Request_Abstract
{
    public function invoiceToRequest($invoice)
    {
        $this->unsetData();
        $this->_prepareData($this->_getInvoiceDataMapping($invoice));
        return $this;
    }

    public function creditmemoToRequest($creditmemo)
    {
        $this->unsetData();
        $this->_prepareData($this->_getCreditmemoDataMapping($creditmemo));
        return $this;
    }
    
    public function orderToRequest(Mage_Sales_Model_Order $order)
    {
        $this->unsetData();
        $this->_prepareData($this->_getOrderDataMapping($order));
    }

    protected function _getInvoiceDataMapping(Mage_Sales_Model_Order_Invoice $invoice)
    {
        return array(
            'date' => time(),
            'terminal' => 99,
            'client_number' => Mage::getModel('ddd_api/config')->getFirstClientId(),
            'note_id' => $invoice->getIncrementId(),
            'terminal' => 99,
            'item_lines' => $this->_createInvoiceItemLines($invoice),
            'payment_lines' => $this->_createInvoicePaymentLines($invoice)
        );
    }

    protected function _createInvoiceItemLines(Mage_Sales_Model_Order_Invoice $invoice)
    {
        $itemLines = array();
        /* @var $invoiceItem Mage_Sales_Model_Order_Invoice_Item */
        foreach ($invoice->getItemsCollection() as $invoiceItem)
        {
            if (!$invoiceItem->getOrderItem()->getParentItemId()) {
                $invoiceItem = Mage::getModel('ddd_api/api_sales_request_item')
                    ->invoiceItemToRequest($invoiceItem);
                    //->getData();
                $itemLines[] = $invoiceItem;
            }
        }
        return $itemLines;
    }

    protected function _createInvoicePaymentLines(Mage_Sales_Model_Order_Invoice $invoice)
    {
        $paymentLines = array();
        $paymentLines[] = Mage::getModel('ddd_api/api_sales_request_payment')
                ->setLineAmount((float) $invoice->getSubtotalInclTax())
                ->setQty(1);
                //->getData();
        if ($discountAmount = (float) $invoice->getDiscountAmount()) {
            $paymentLines[] = Mage::getModel('ddd_api/api_sales_request_payment')
                ->setLineAmount($discountAmount * (-1))
                ->setQty(1);
                //->getData();
        }
        return $paymentLines;
    }

    protected function _getCreditmemoDataMapping(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
        return array(
            'date' => time(),
            'terminal' => 99,
            'client_number' => Mage::getModel('ddd_api/config')->getFirstClientId(),
            'note_id' => $creditmemo->getIncrementId(),
            'item_lines' => $this->_createCreditmemoItemLines($creditmemo),
            'payment_lines' => $this->_createCreditmemoPaymentLines($creditmemo)
        );
    }

    protected function _createCreditmemoItemLines(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
        $itemLines = array();
        /* @var $creditmemoItem Mage_Sales_Model_Order_Creditmemo_Item */
        foreach ($creditmemo->getItemsCollection() as $creditmemoItem)
        {
            if (!$creditmemoItem->getOrderItem()->getParentItemId()) {
                $creditmemoItem = Mage::getModel('ddd_api/api_sales_request_item')
                    ->creditmemoItemToRequest($creditmemoItem);
                    //->getData();
                $itemLines[] = $creditmemoItem;
            }
        }
        return $itemLines;
    }

    protected function _createCreditmemoPaymentLines(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
        $paymentLines = array();
        $paymentLines[] = Mage::getModel('ddd_api/api_sales_request_payment')
            ->setLineAmount((float) $creditmemo->getSubtotalInclTax())
            ->setQty(1);
            //->getData();
        if ($discountAmount = (float) $creditmemo->getDiscountAmount()) {
            $paymentLines[] = Mage::getModel('ddd_api/api_sales_request_payment')
                ->setLineAmount($discountAmount * (-1))
                ->setQty(1);
                //->getData();
        }
        return $paymentLines;
    }
    
    protected function _getOrderDataMapping(Mage_Sales_Model_Order $order)
    {
        $noteId = $order->getIncrementId();
        if ($order->getState() == Mage_Sales_Model_Order::STATE_CANCELED) {
            $noteId = 'b' . substr(microtime(), 2, 7);
        }
        return array(
            'date' => time(),
            'terminal' => 99,
            'client_number' => Mage::getModel('ddd_api/config')->getFirstClientId(),
            'note_id' => $noteId,
            'terminal' => 99,
            'item_lines' => $this->_createOrderItemLines($order),
            'payment_lines' => $this->_createOrderPaymentLines($order)
        );
    }
    
    protected function _createOrderItemLines(Mage_Sales_Model_Order $order)
    {
        $itemLines = array();
        /* @var $orderItem Mage_Sales_Model_Order_Item */
        foreach ($order->getItemsCollection() as $orderItem)
        {
            if (!$orderItem->getParentItemId()) {
                $orderItem = Mage::getModel('ddd_api/api_sales_request_item')
                    ->orderItemToRequest($orderItem);
                    //->getData();
                $itemLines[] = $orderItem;
            }
        }
        return $itemLines;
    }
    
    protected function _createOrderPaymentLines(Mage_Sales_Model_Order $order)
    {
        $paymentLines = array();
        $paymentLines[] = Mage::getModel('ddd_api/api_sales_request_payment')
            ->setLineAmount((float) $order->getSubtotalInclTax())
            ->setQty(1);
            //->getData();
        if ($discountAmount = (float) $order->getDiscountAmount()) {
            $paymentLines[] = Mage::getModel('ddd_api/api_sales_request_payment')
            ->setLineAmount($discountAmount * (-1))
            ->setQty(1);
            //->getData();
        }
        return $paymentLines;
    }
}