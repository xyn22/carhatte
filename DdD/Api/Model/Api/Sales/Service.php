<?php

/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api_Sales_Service
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */
class DdD_Api_Model_Api_Sales_Service extends SoapClient
{
    protected $_defaultWsdl = 'http://api.dddadmin.com/SaleService.svc?wsdl';

    public function __construct($options = array())
    {
        $wsdl = $this->_defaultWsdl;
        if (isset($options['wsdl'])) {
            $wsdl = $options['wsdl'];
            unset($options['wsdl']);
        }
        $options['classmap'] = array(
            'Sale' => 'DdD_Api_Model_Api_Sales_Request',
            'ItemLine' => 'DdD_Api_Model_Api_Sales_Request_Item',
            'PaymentLine' => 'DdD_Api_Model_Api_Sales_Request_Payment'
        );
        parent::__construct($wsdl, $options);
    }

    public function createRequest()
    {
        $object = $this->__soapCall('StartSale', array(), array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
        ));
        return $object->StartSaleResult;
    }

    public function processRequest($request)
    {
        Mage::log(json_encode($request->getData()));
        foreach ($request->ItemLines as $item) {
            Mage::log(json_encode($item->getData()));
        }
        foreach ($request->PaymentLines as $item) {
            Mage::log(json_encode($item->getData()));
        }
        $parameters = array(
            'sale' => $request,
            'psk' => $this->_getApiKey()
        );
        $response = $this->__soapCall('SaveSale', array($parameters), array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
        ));
        Mage::log(json_encode($response));
        if (!$response->SaveSaleResult->Saved) {
            Mage::throwException($response->SaveSaleResult->Message);
        }
    }

    public function getError()
    {
        return $this->_error;
    }

    protected function _getApiKey()
    {
        return Mage::getModel('ddd_api/config')->getApiKey();
    }
}