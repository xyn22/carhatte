<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api_Model_Sales_Order_Observer
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Api_Model_Sales_Order_Observer
{
    public function paySalesOrder(Varien_Event_Observer $observer)
    {
        if (!$this->_isSalesServiceEnabled()) {
            Mage::log('Sale service disabled');
            return;
        }
        Mage::log('DdD_Api_Model_Sales_Observer::paySaleOrder');
        /* @var $invoice Mage_Sales_Model_Order_Invoice */
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        $helper = Mage::helper('ddd_api');
        try {
            $service = Mage::getModel('ddd_api/api_sales_service');
            $request = $service->createRequest();
            $request->invoiceToRequest($invoice);
            $service->processRequest($request);
            $order->addStatusHistoryComment($helper->__("Invoiced items has been saved in DdD (Invoice ID: {$invoice->getIncrementId()})"));
        } catch (Exception $e) {
            $order->addStatusHistoryComment(vsprintf($helper->__('Error while invoiced items were saving in DdD "%s"'), $e->getMessage()), $order->getStatus());
            Mage::logException($e);
            Mage::throwException($e->getMessage());
        }
    }
    
    public function placeSalesOrder(Varien_Event_Observer $observer)
    {
        
        if (!$this->_isSalesServiceEnabled()) {
            Mage::log('Sale service disabled');
            return;
        }
        Mage::log('DdD_Api_Model_Sales_Observer::placeSaleOrder');
        /* @var $order Mage_Sales_Model_Order */
        $order = $observer->getEvent()->getOrder();
        Mage::log(json_encode(array($order->getStatus(), $order->getState())));
        $helper = Mage::helper('ddd_api');
        try {
            $service = Mage::getModel('ddd_api/api_sales_service');
            $request = $service->createRequest();
            $request->orderToRequest($order);
            $service->processRequest($request);
            $order->addStatusHistoryComment($helper->__("Ordered items has been saved in DdD"));
        } catch (Exception $e) {
            $order->addStatusHistoryComment(vsprintf($helper->__('Error while ordered items were saving in DdD "%s"'), $e->getMessage()), $order->getStatus());
            Mage::logException($e);
            Mage::throwException($e->getMessage());
        }
    }
    
    public function refundSalesOrder(Varien_Event_Observer $observer)
    {
        if (!$this->_isSalesServiceEnabled()) {
            Mage::log('Sale service disabled');
            return;
        }
        Mage::log('DdD_Api_Model_Sales_Observer::refundSaleOrder');
        /* @var $creditmemo Mage_Sales_Model_Order_Creditmemo */
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order = $creditmemo->getOrder();
        $helper = Mage::helper('ddd_api');
        try {
            $service = Mage::getModel('ddd_api/api_sales_service');
            $request = $service->createRequest();
            $request->creditmemoToRequest($creditmemo);
            $service->processRequest($request);
            $order->addStatusHistoryComment($helper->__("Refunded items has been saved in DdD (Creditmemo ID: {$creditmemo->getIncrementId()})"));
        } catch (Exception $e) {
            $order->addStatusHistoryComment(vsprintf($helper->__('Error while refunded items were saving in DdD "%s"'), $e->getMessage()), $order->getStatus());
            Mage::logException($e);
            Mage::throwException($e->getMessage());
        }
    }

    protected function _isSalesServiceEnabled()
    {
        return Mage::getModel('ddd_api/config')->isSaleServiceEnabled();
    }
}