<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api_Model_Import
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Api_Model_Import extends Varien_Object
{
    const CONFIG_KEY_ENTITIES  = 'global/ddd_api/import_entities';
    
    const METHOD_IMPORT_ALL = 1;
    const METHOD_IMPORT_PARTIAL = 2;

    protected $_importEntityModel;

    public function importSource()
    {
        $importEntityModel = $this->_getImportEntityModel();
        $importEntityModel->importData();
        return $importEntityModel->getStatus();
    }

    protected function _getImportEntityModel()
    {
        if (null !== $this->_importEntityModel) {
            return $this->_importEntityModel;
        }
        $validTypes = $this->_getModels(static::CONFIG_KEY_ENTITIES);
        if (isset($validTypes[$this->getEntity()]['model'])) {
            try {
                $this->_importEntityModel = Mage::getModel($validTypes[$this->getEntity()]['model'])
                    ->setData($this->getData());
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::throwException(Mage::helper('ddd_api')->__('Invalid entity model'));
            }
        } else {
            Mage::throwException('Invalid entity type');
        }
        return $this->_importEntityModel;
    }

    protected function _getModels($configKey)
    {
        $entities = array();
        foreach (Mage::getConfig()->getNode($configKey)->asCanonicalArray() as $entityType => $entityParams) {
            if (empty($entityParams['model_token'])) {
                Mage::throwException(Mage::helper('ddd_api')->__('Node does not has model token tag'));
            }
            $entities[$entityType] = array(
                'model' => $entityParams['model_token'],
                'label' => empty($entityParams['label']) ? $entityType : $entityParams['label']
            );
        }
        return $entities;
    }
}