<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api_Model_Observer
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Api_Model_Observer
{
    public function importProducts()
    {
        $config = $this->_getConfigModel();
        if (!$config->isCronEnabled()) {
            Mage::log('Cron is disabled');
            return;
        }
        if (!$config->isApiEnabled()) {
            Mage::log('Api is disabled');
            return;
        }
        Mage::log('Start import product');
        Mage::log("CLient ID: {$config->getFirstClientId()}");
        if ($clientId = $config->getFirstClientId()) {
            $importModel = $this->_getImportProductModel()
                ->setMethod(DdD_Api_Model_Import::METHOD_IMPORT_ALL)
                ->setNewOnly($config->newProductsOnly());
            $this->_importModel($importModel, $clientId);
        }
        Mage::log('End import product');
    }

    public function importStock()
    {
        $config = $this->_getConfigModel();
        if (!$config->isCronEnabled()) {
            Mage::log('Cron is disabled');
            return;
        }
        if (!$config->isStockImportEnabled()) {
            Mage::log('Stock Import is disabled');
            return;
        }
        Mage::log('Start import stock');
        $clientId = $this->_getStockClientId();
        Mage::log("CLient ID: {$clientId}");
        if ($clientId) {
            $importModel = $this->_getImportStockModel();
            $this->_importModel($importModel, $clientId);
        }
        Mage::log('End import stock');
    }

    protected function _importModel(DdD_Api_Model_Import_Abstract $importModel, $clientId)
    {
        try {
            $importModel->setClientId($clientId)
                ->setCount(0);
            $start = 0; $step = 2000;
            do {
                $importModel->setStepStart($start)
                    ->setStep($step)
                    ->importData();
                $start += $step;
                Mage::log("Next Start: {$start}");
            } while(!$importModel->isImportCompleted());
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * @return DdD_Api_Model_Config
     */
    protected function _getConfigModel()
    {
        return Mage::getModel('ddd_api/config');
    }

    /**
     * @return DdD_Api_Model_Import_Product
     */
    protected function _getImportProductModel()
    {
        return Mage::getModel('ddd_api/import_product');
    }

    /**
     * @return DdD_Api_Model_Import_Stock
     */
    protected function _getImportStockModel()
    {
        return Mage::getModel('ddd_api/import_stock');
    }

    protected function _getStockClientId()
    {
        return Mage::helper('ddd_api')->getClientIds();
    }
}