<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   Ddd_Service
 * @package    Controller
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class Ddd_Service_ImportController extends Mage_Core_Controller_Front_Action
{

    protected function _construct()
    {
        Mage::getSingleton('core/session', array(
            'name' => 'adminhtml'
        ));
        $session = Mage::getSingleton('admin/session');
        if (!$session->isLoggedIn()) {
            $this->_redirect('adminhtml');
        }
    }

    public function indexAction()
    {
        $this->_getStatusHelper()->resetStatus();
        $this->loadLayout();
        $block = $this->getLayout()->createBlock('Mage_Core_Block_Template', 'dddimport', array(
            'template' => 'dddservice/import/product.phtml'
        ));
        $import_all = $this->getRequest()->getParam('import_all');
        $start = (int) Mage::getStoreConfig('settings/import/start');
        $limit = (int) Mage::getStoreConfig('settings/import/limit');
        if ($import_all) {
            $start = 0;
            $limit = 0;
        }
        $block->addData(compact('import_all', 'start', 'limit'));
        $this->getLayout()
            ->getBlock('content')
            ->append($block);
        $this->renderLayout();
    }

    public function productAction()
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        try {
            $start = $this->getRequest()->getParam('start', 0);
            $limit = $this->getRequest()->getParam('limit', 0);
            $step = $this->getRequest()->getParam('step', 500);
            $clientId = Mage::getStoreConfig('settings/general/clientid');
            $importAll = $this->getRequest()->getParam('import_all');
            $statusHelper = $this->_getStatusHelper();
            //Mage::log('Start Controller: ' . $start);
            $service = Mage::getModel('dddservice/import_product')
                ->setClientId($clientId)
                ->setImportAll($importAll)
                ->setStart($start)
                ->setLimit($limit)
                ->setStep($step)
                ->setCount($statusHelper->getStatusData('count', 0))
                ->applyFilter('_importProduct', array($statusHelper, 'updateStatus'))
                ->applyFilter('_runIndexer', array($statusHelper, 'updateStatus'))
                ->applyFilter('_importStock', array($statusHelper, 'updateStatus'))
                ->applyFilter('_importCompleted', array($statusHelper, 'completeStatus'))
                ->importData();
        } catch (Exception $e) {
            Mage::logException($e);
            $error = $e->getMessage();
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(json_encode(compact('error')));
    }

    public function importstockAction()
    {
        $clientId = Mage::getStoreConfig('settings/general/clientid');
        $importModel = Mage::getModel('dddservice/import_stock')
            ->setClientId($clientId)
            ->importData();
    }

    public function statusAction()
    {
        $statusHelper = $this->_getStatusHelper();
        $statusJson = $statusHelper->toJson();
        $statusHelper->resetStatusData('messages');
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($statusJson);
    }

    /**
     * @return Ddd_Service_Helper_Status
     */
    protected function _getStatusHelper()
    {
        return Mage::helper('dddservice/status')
            ->setStatusKey(Ddd_Service_Model_Import_Product::STATUS_KEY);
    }
}

/*require_once 'Ddd/Service/Model/SaleService.php';

class Ddd_Service_ImportController extends Mage_Core_Controller_Front_Action {

	public function indexAction() {


		echo "START<br><br>";

        $service = Mage::getModel('dddservice/productservice');

        //$service->setUseChache(true);

        $start = intval(Mage::getStoreConfig('settings/import/start'));
        $limit = intval(Mage::getStoreConfig('settings/import/limit'));

        $error = '';

        if (Mage::app()->getRequest()->getParam('only_new', false)){
           $service->setOnlyNew(true);
        }
        if (Mage::app()->getRequest()->getParam('import_all', false)){
           $service->setImportAll(true);
        }
        if (Mage::app()->getRequest()->getParam('start', 0)){
          $start = intval(Mage::app()->getRequest()->getParam('start', 0));
        }
        if (Mage::app()->getRequest()->getParam('limit', 0)){
          $limit = intval(Mage::app()->getRequest()->getParam('limit', 0));
        }

        if (!$start){
          $start = 1;
        }


        $clientIds = Mage::getStoreConfig('settings/general/clientids');
        $idsArr = explode(",", trim($clientIds));


        if ( $idsArr )
        {
            foreach( $idsArr as $id )
            {
                $service->_client_id = $id;

                try{

                    $service->importProduct($start, $limit);
                }catch (Exception $e){
                    $error = $e->getMessage();
                }
            }
        }



        echo "FINISH<br><br>";

        if ($error){
          echo "ERROR: " . $error . "<br><br>";
        }else{
          if (Mage::app()->getRequest()->getParam('import_all', false)){
            $message = "All products was imported";
          }
          else{
            $message = "Products " . $start . " - " . ($limit ? $start + $limit : "end of products") . " was imported";
          }
          echo $message . "<br><br>";
          $email = Mage::getStoreConfig('settings/import/email');
          if ($email){
            $mail = Mage::getModel('core/email');
            $mail->setToName('Admin');
            $mail->setToEmail($email);
            $mail->setBody($message);
            $mail->setSubject('DDD Import');
            $mail->setFromEmail('cr@sforsneakers.com');
            $mail->setFromName('SforSneakers - Info');
            $mail->setType('text');
            try {
              $mail->send();
            }
            catch (Exception $e) {}
          }
        }


//        $indexingProcesses = Mage::getSingleton('index/indexer')->getProcessesCollection();
//        foreach ($indexingProcesses as $process) {
//              $process->reindexEverything();
//        }


//        echo "Reindexed data";

        echo "Data were not reindexed. Do it manually.";

        exit();

	}

    public function infoAction() {

        $service = Mage::getModel('dddservice/productservice');

        $clientIds = Mage::getStoreConfig('settings/general/clientids');
        $idsArr = explode(",", trim($clientIds));


        if ( $idsArr )
        {
            foreach( $idsArr as $id )
            {
                $service->_client_id = $id;

                echo "<pre>";
                print_r($service->getProducts());
                echo "</pre>";

                $count = count($service->getProducts());
                echo $count;
            }
        }

        exit();

    }

    public function newproductsAction() {
        $service = Mage::getModel('dddservice/productservice');

        $service->setOnlyNew(true);
        //$service->setUseChache(true);


        $clientIds = Mage::getStoreConfig('settings/general/clientids');
        $idsArr = explode(",", trim($clientIds));

        $this->loadLayout();

        if ( $idsArr )
        {
            foreach( $idsArr as $id )
            {
               $service->_client_id = (int)$id;

                $products = $service->getProducts();

                $block = $this->getLayout()->createBlock(
                    'core/template',
                    'dddservice_newproducts',
                    array('template' => 'dddservice/newproducts.phtml')
                );
                $block->setProducts($products);
                $this->getLayout()->getBlock('content')->append($block);

            }
        }

        $this->renderLayout();
    }

    public function testAction(){

        $ean = Mage::app()->getRequest()->getParam('ean', false);
        if ($ean) {
            $service = Mage::getModel('dddservice/productservice');
            echo "QTY:" . $service->getStockCount($ean);
        }
        exit();

    }

    public function testinfoAction(){

        $service = Mage::getModel('dddservice/productservice');

        $clientIds = Mage::getStoreConfig('settings/general/clientids');
        $idsArr = explode(",", trim($clientIds));


        if ( $idsArr )
        {
            foreach( $idsArr as $id )
            {
                $service->_client_id = $id;

                //$service->only_new = false;
                $x = $service->getProducts();

                echo '<pre>';
                var_dump($x);
                echo '</pre>';
            }
        }


        exit;

    }

    public function testcountAction()
    {

        $service = Mage::getModel('dddservice/productservice');
        $service->testCount();



    }

    public function stockAction(){
        $service = Mage::getModel('dddservice/service');
        echo "START<br>";

        $service->runStockImport();

        echo "FINISH<br>";
        exit();
    }
}*/