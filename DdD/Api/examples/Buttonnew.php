<?php

class Ddd_Service_Block_Buttonnew extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $url = $this->getUrl('dddservice/import/newproducts');

        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setType('button')
                    ->setClass('scalable save')
                    ->setLabel('Show')
                    ->setOnClick("javascript:window.open('$url')")
                    ->toHtml();

        return $html;

    }
}