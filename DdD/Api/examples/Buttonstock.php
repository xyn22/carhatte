<?php

class Ddd_Service_Block_Buttonstock extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $url = $this->getUrl('dddservice/import/stock');

        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setType('button')
                    ->setClass('scalable save')
                    ->setLabel('Update')
                    ->setOnClick("javascript:window.open('$url')")
                    ->toHtml();

        return $html;

    }
}