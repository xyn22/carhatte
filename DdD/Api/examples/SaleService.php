<?php

class Sale{
  public $Type;
  public $NoteID;
  public $Date;
  public $Employee;
  public $ClientNumber;
  public $Terminal;
  public $ItemLines = array();
  public $PaymentLines = array();
  public $SaleTotal;
}

class ItemLine{
  public $Type;
  public $ItemGroup;
  public $Supplier;
  public $Qty;
  public $LineAmount;
  public $DiscountAmount;
  public $EDBNumber;
  public $TransferToStore;
  public $DiscountCode;
  public $ReturnCode;
  public $CustomerNumber;
  public $DateTime;
  public $GiftCertificate;
  public $BarcodeUsed;
}

class PaymentLine {
  public $CardNumber;
  public $CurrencyCode;
  public $Qty;
  public $LineAmount;
  public $GiftCertificate;
}

class SaleService extends SoapClient {

  private static $classmap = array(
                                'Sale' => 'Sale',
                                'ItemLine' => 'ItemLine',
                                'PaymentLine' => 'PaymentLine',
                            );

  public function SaleService($wsdl = "http://api.dddadmin.com/SaleService.svc?wsdl", $options = array()) {

    $options['classmap'] = array();

    foreach(self::$classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }

    parent::__construct($wsdl, $options);
  }


  public function StartSale() {
    return $this->__soapCall('StartSale', array(), array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  public function SaveSale(Sale $sale, $psk) {
    $parameters = array('sale' => $sale, 'psk' => $psk);
    return $this->__soapCall('SaveSale', array($parameters), array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

}

?>
