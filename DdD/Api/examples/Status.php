<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   Ddd
 * @package    Ddd_Service_Helper_Status
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class Ddd_Service_Helper_Status extends Mage_Core_Helper_Abstract
{
    const SESSION_KEY_PREFX = 'status';
    
    protected $_statusKey;

    public function setStatusKey($statusKey)
    {
        $this->_statusKey = $statusKey;
        return $this;
    }
    
    public function updateStatus(Ddd_Service_Model_Import_Abstract $import, Ddd_Service_Model_Filter $filter, $params)
    {
        $status = (array) $this->getStatus();
        $defaults = array(
            'total' => $import->getTotal(),
            'messages' => array(),
        );
        $status += $defaults;
        $status['count'] = $import->getCount();
        $status['messages'] = $import->getMessages();
        //Mage::log('Update status: ' . json_encode($status));
        $this->setStatus($status);
        return $filter->next($import, $filter, $params);
    }

    public function completeStatus(Ddd_Service_Model_Import_Abstract $importModel, Ddd_Service_Model_Filter $filter, $params)
    {
        $this->setStatus(array(
            'completed' => true,
            'count' => $importModel->getCount(),
            'total' => $importModel->getTotal(),
            'messages' => $importModel->getMessages()
        ));
        return $filter->next($importModel, $filter, $params);
    }

    public function getStatusData($dataKey, $default = null, $statusKey = null)
    {
        $status = $this->getStatus($statusKey);
        if (is_array($dataKey)) {
            $statusData = array();
            foreach($dataKey as $oneDataKey) {
                $oneStatusData = $this->getStatusData($oneDataKey);
                if (null !== $oneStatusData) {
                    $statusData[$oneDataKey] = $this->getStatusData($oneDataKey);
                }
            }
            return $statusData;
        }
        if (isset($status[$dataKey])) {
            return $status[$dataKey];
        }
        return $default;
    }

    public function setStatusData($dataKey, $dataValue, $statusKey = null)
    {
        $status = $this->getStatus($statusKey);
        $status[$dataKey] = $dataValue;
        return $this->setStatus($status, $statusKey);
    }

    public function resetStatusData($dataKeys, $statusKey = null)
    {
        if (is_string($dataKeys)) {
            $dataKeys = array($dataKeys);
        }
        if (!is_array($dataKeys)) {
            Mage::throwException('Invalid status data keys!');
        }
        $status = (array) $this->getStatus($statusKey);
        foreach ($dataKeys as $dataKey) {
            if (isset($status[$dataKey])) {
                unset($status[$dataKey]);
            }
        }
        return $this->setStatus($status, $statusKey);
    }

    public function getStatus($statusKey = null)
    {
        $statusKey = $this->_createStatusKey($statusKey);
        $session = Mage::getSingleton('core/session');
        return $session->getData($statusKey);
    }

    public function setStatus(array $status, $statusKey = null)
    {
        $statusKey = $this->_createStatusKey($statusKey);
        $session = Mage::getSingleton('core/session');
        $session->setData($statusKey, $status);
        return $this;
    }

    public function resetStatus($statusKey = null)
    {
        $statusKey = $this->_createStatusKey($statusKey);
        $session = Mage::getSingleton('core/session');
        $session->unsetData($statusKey);
        return $this;
    }
    
    public function toJson($statusKey = null)
    {
        return json_encode($this->getStatus($statusKey));
    }

    public function getMemoryUsage()
    {
        $size = memory_get_usage(true);
        $unit=array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . $unit[$i];
    }

    protected function _createStatusKey($statusKey = null)
    {
        if (null === $statusKey) {
            $statusKey = $this->_statusKey;
        }
        if (strpos($statusKey, static::SESSION_KEY_PREFX . '.') !== 0) {
            $statusKey = static::SESSION_KEY_PREFX . '.' . $statusKey;
        } 
        return $statusKey;
    }
}