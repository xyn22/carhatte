<?php

class Ddd_Service_Block_Buttonall extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $url = $this->getUrl('dddservice/import/index', array('import_all' => 1, 'start' => 1));

        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setType('button')
                    ->setClass('scalable save')
                    ->setLabel('Run Import')
                    ->setOnClick("javascript:window.open('$url')")
                    ->toHtml();

        return $html;
    }
}