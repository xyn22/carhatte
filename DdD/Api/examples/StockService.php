<?php
class char {
}

class duration {
}

class guid {
}

class DeliveryNote {
  public $DeliveryDate; // dateTime
  public $DeliveryNoteId; // string
  public $ImportListNo; // int
  public $IncludeZeroLinesInDraft; // boolean
  public $OrderReference; // string
  public $Recipient; // int
  public $State; // DeliveryNoteNoteState
}

class ArticleHeader {
  public $ClientId; // int
  public $Date; // dateTime
  public $Lines; // ArrayOfArticleLine
  public $MatchOnEan; // boolean
}

class ArticleLine2 {
  public $CostpriceEach; // decimal
  public $Ean; // long
  public $EdbNumber; // int
  public $ItemGroup; // int
  public $ItemGroupName; // string
  public $KatalogNumber; // int
  public $Kparam1; // string
  public $Kparam2; // string
  public $Kparam3; // string
  public $Kparam4; // string
  public $Kparam5; // string
  public $Qty; // int
  public $SalesPriceEach; // decimal
  public $Supplier; // int
  public $SupplierName; // string
  public $Vparam1; // string
  public $Vparam2; // string
  public $Vparam3; // string
  public $Vparam4; // string
  public $Vparam5; // string
}

class DeliveryNoteNoteState {
}

class ArticleResponse {
  public $Message; // string
  public $Response; // ArticleResponseStatus
}

class ArticleResponseStatus {
}

class PriceChange {
  public $ClientId; // int
  public $Ean; // long
  public $NewCostPriceEach; // decimal
  public $NewSalesPriceEach; // decimal
  public $TypeOfChange; // PriceChangeType
}

class PriceChangeType {
}

class POSInformation {
  public $Identifier; // int
  public $Name; // string
  public $Number; // int
  public $TypeOfInformation; // POSInformationInformationType
}

class POSInformationInformationType {
}

class KeyValueOflongint {
  public $Key; // long
  public $Value; // int
}

class ArticleLine {
  public $CatalogueParameter1; // string
  public $CatalogueParameter2; // string
  public $CatalogueParameter3; // string
  public $CatalogueParameter4; // string
  public $CatalogueParameter5; // string
  public $ClientNoRecipient; // string
  public $CostpriceEach; // string
  public $DeliveryDate; // string
  public $Ean; // string
  public $ErrorDescriptionsCurrentLine; // ArrayOfstring
  public $ErrorsInLine; // boolean
  public $InvoiceNo; // string
  public $ItemgroupDescription; // string
  public $ItemgroupNo; // string
  public $LineCount; // int
  public $OriginalLine; // string
  public $Quantity; // string
  public $SalespriceEach; // string
  public $SupplierName; // string
  public $SupplierNo; // string
  public $SupplierOrderNo; // string
  public $SupplierTelephone; // string
  public $VariantParameter1; // string
  public $VariantParameter2; // string
  public $VariantParameter3; // string
  public $VariantParameter4; // string
  public $VariantParameter5; // string
  public $WarningDescriptionsCurrentLine; // ArrayOfstring
  public $WarningsInLine; // boolean
}

class MakeDeliveryNote {
  public $dn; // DeliveryNote
  public $client; // int
  public $psk; // string
}

class MakeDeliveryNoteResponse {
  public $MakeDeliveryNoteResult; // ArticleResponse
}

class ArticleAveragePrice {
  public $supplier; // int
  public $edbnumber; // string
  public $client; // int
  public $psk; // string
}

class ArticleAveragePriceResponse {
  public $ArticleAveragePriceResult; // decimal
}

class PriceChange2 {
  public $client; // int
  public $priceChanges; // ArrayOfPriceChange
  public $psk; // string
}

class PriceChangeResponse {
  public $PriceChangeResult; // ArticleResponse
}

class StockCount {
  public $eans; // ArrayOflong
  public $client; // int
  public $psk; // string
}

class StockCountResponse {
  public $StockCountResult; // ArrayOfKeyValueOflongint
}

class StockCountSingleEan{
  public $ean; // long
  public $client; // int
  public $psk; // string
}

class StockCountSingleEanResponse {
  public $StockCountSingleEanResult;
}

class StockCountByParameter {
  public $eans; // ArrayOflong
  public $client; // int
  public $psk; // string
}

class StockCountByParameterResponse {
  public $StockCountByParameterResult; // ArrayOfKeyValueOflongint
}

class GetFirstMachineNumber {
  public $clientid; // int
  public $psk; // string
}

class GetFirstMachineNumberResponse {
  public $GetFirstMachineNumberResult; // int
}

class GetLastItemGroupIncludedInSale {
  public $clientid; // int
  public $psk; // string
}

class GetLastItemGroupIncludedInSaleResponse {
  public $GetLastItemGroupIncludedInSaleResult; // int
}

class GetClerkAndShopInfo {
  public $clientid; // int
  public $onlyShopInfo; // boolean
  public $psk; // string
}

class GetClerkAndShopInfoResponse {
  public $GetClerkAndShopInfoResult; // ArrayOfPOSInformation
}

class GetWebshopArticles {
  public $clientid; // int
  public $psk; // string
}

class GetWebshopArticlesResponse {
  public $GetWebshopArticlesResult; // string
}

class ValidateArticleFile {
  public $koncern; // short
  public $lines; // ArrayOfstring
  public $levConvertList; // short
  public $grpConvertList; // short
  public $decimalSeparator; // string
}

class ValidateArticleFileResponse {
  public $ValidateArticleFileResult; // ArrayOfArticleLine
}


/**
 * StockService class
 *
 *  
 * 
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class StockService extends SoapClient {

  private static $classmap = array(
                                    'char' => 'char',
                                    'duration' => 'duration',
                                    'guid' => 'guid',
                                    'DeliveryNote' => 'DeliveryNote',
                                    'ArticleHeader' => 'ArticleHeader',
                                    'ArticleLine' => 'ArticleLine',
                                    'DeliveryNoteNoteState' => 'DeliveryNoteNoteState',
                                    'ArticleResponse' => 'ArticleResponse',
                                    'ArticleResponseStatus' => 'ArticleResponseStatus',
                                    'PriceChange' => 'PriceChange',
                                    'PriceChangeType' => 'PriceChangeType',
                                    'POSInformation' => 'POSInformation',
                                    'POSInformationInformationType' => 'POSInformationInformationType',
                                    'KeyValueOflongint' => 'KeyValueOflongint',
                                    'ArticleLine2' => 'ArticleLine2',
                                    'MakeDeliveryNote' => 'MakeDeliveryNote',
                                    'MakeDeliveryNoteResponse' => 'MakeDeliveryNoteResponse',
                                    'ArticleAveragePrice' => 'ArticleAveragePrice',
                                    'ArticleAveragePriceResponse' => 'ArticleAveragePriceResponse',
                                    'PriceChange2' => 'PriceChange2',
                                    'PriceChangeResponse' => 'PriceChangeResponse',
                                    'StockCount' => 'StockCount',
                                    'StockCountResponse' => 'StockCountResponse',
                                    'StockCountSingleEan' => 'StockCountSingleEan',
                                    'StockCountSingleEanResponse' => 'StockCountSingleEanResponse',
                                    'StockCountByParameter' => 'StockCountByParameter',
                                    'StockCountByParameterResponse' => 'StockCountByParameterResponse',
                                    'GetFirstMachineNumber' => 'GetFirstMachineNumber',
                                    'GetFirstMachineNumberResponse' => 'GetFirstMachineNumberResponse',
                                    'GetLastItemGroupIncludedInSale' => 'GetLastItemGroupIncludedInSale',
                                    'GetLastItemGroupIncludedInSaleResponse' => 'GetLastItemGroupIncludedInSaleResponse',
                                    'GetClerkAndShopInfo' => 'GetClerkAndShopInfo',
                                    'GetClerkAndShopInfoResponse' => 'GetClerkAndShopInfoResponse',
                                    'GetWebshopArticles' => 'GetWebshopArticles',
                                    'GetWebshopArticlesResponse' => 'GetWebshopArticlesResponse',
                                    'ValidateArticleFile' => 'ValidateArticleFile',
                                    'ValidateArticleFileResponse' => 'ValidateArticleFileResponse',
                                   );

    public function StockService($wsdl = "http://api.dddadmin.com/StockService.svc?wsdl", $options = array()) {
    foreach(self::$classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    parent::__construct($wsdl, $options);
  }

  /**
   *  
   *
   * @param MakeDeliveryNote $parameters
   * @return MakeDeliveryNoteResponse
   */
  public function MakeDeliveryNote(MakeDeliveryNote $parameters) {
    return $this->__soapCall('MakeDeliveryNote', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param ArticleAveragePrice $parameters
   * @return ArticleAveragePriceResponse
   */
  public function ArticleAveragePrice(ArticleAveragePrice $parameters) {
    return $this->__soapCall('ArticleAveragePrice', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param PriceChange $parameters
   * @return PriceChangeResponse
   */
  public function PriceChange(PriceChange $parameters) {
    return $this->__soapCall('PriceChange', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param StockCount $parameters
   * @return StockCountResponse
   */
  public function StockCount(StockCount $parameters) {

    return $this->__soapCall('StockCount', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param StockCountSingleEan $parameters
   * @return StockCountSingleEanResponse
   */
  public function StockCountSingleEan(StockCountSingleEan $parameters) {
    return $this->__soapCall('StockCountSingleEan', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param StockCountByParameter $parameters
   * @return StockCountByParameterResponse
   */
  public function StockCountByParameter(StockCountByParameter $parameters) {
    return $this->__soapCall('StockCountByParameter', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetFirstMachineNumber $parameters
   * @return GetFirstMachineNumberResponse
   */
  public function GetFirstMachineNumber(GetFirstMachineNumber $parameters) {
    return $this->__soapCall('GetFirstMachineNumber', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetLastItemGroupIncludedInSale $parameters
   * @return GetLastItemGroupIncludedInSaleResponse
   */
  public function GetLastItemGroupIncludedInSale(GetLastItemGroupIncludedInSale $parameters) {
    return $this->__soapCall('GetLastItemGroupIncludedInSale', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetClerkAndShopInfo $parameters
   * @return GetClerkAndShopInfoResponse
   */
  public function GetClerkAndShopInfo(GetClerkAndShopInfo $parameters) {
    return $this->__soapCall('GetClerkAndShopInfo', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetWebshopArticles $parameters
   * @return GetWebshopArticlesResponse
   */
  public function GetWebshopArticles(GetWebshopArticles $parameters) {
    return $this->__soapCall('GetWebshopArticles', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param ValidateArticleFile $parameters
   * @return ValidateArticleFileResponse
   */
  public function ValidateArticleFile(ValidateArticleFile $parameters) {
    return $this->__soapCall('ValidateArticleFile', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

}

?>
