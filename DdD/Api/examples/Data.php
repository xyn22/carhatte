<?php

class Ddd_Service_Helper_Data extends Mage_Core_Helper_Abstract
{
	private static $console;
	private static $filename = "ddd_service.log";
	
	private static $debug = false;
	
	public function getBaseDir()
	{
        return Mage::getBaseDir() . '/app/code/local/Ddd/';
	}
    
    public function getLogDir()
	{
        return Mage::getBaseDir() . '/var/log/';
	}
    
    public function getConfig()
    {
        $config = array();
        
        $config['general']['enabled'] = Mage::getStoreConfig('settings/general/enabled');
        $config['general']['stock_import_enabled'] = Mage::getStoreConfig('settings/general/stock_import_enabled');
        //$config['general']['store'] = Mage::getStoreConfig('settings/general/store');
        $config['general']['clientid'] = Mage::getStoreConfig('settings/general/clientid');
        $config['general']['key'] = Mage::getStoreConfig('settings/general/key');
        $config['cron']['stock_import_period'] = Mage::getStoreConfig('settings/cron/stock_import_period');
        
        return $config;
    }
    
    public function getStore()
    {
        $storeId = $this->getConfig();
        $storeId = $storeId['general']['store'];
        if (!$storeId) {
            throw new Exception('No store configured');
        }
        $store = Mage::getModel('core/store')->load($storeId);
        if (!$store) {
            throw new Exception('No valid store configured');
        }
        return $store;
    }
    
    public function info($msg, $m1 = '', $m2 = '', $m3 = '', $m4 = '', $m5 = '', $m6 = '') 
    {
        if ($this->_logging()) {
            $msg = "info: " . print_r($msg, true) . print_r($m1, true) . print_r($m2, true) . print_r($m3, true) . print_r($m4, true) . print_r($m5, true) . print_r($m6, true);
            $this->_write($msg);
        }
    }
    
    public function warning($msg, $m1 = '', $m2 = '', $m3 = '', $m4 = '', $m5 = '', $m6 = '') 
    {
        if ($this->_logging()) {
            $msg = "warning: " . print_r($msg, true) . print_r($m1, true) . print_r($m2, true) . print_r($m3, true) . print_r($m4, true) . print_r($m5, true) . print_r($m6, true);
            $this->_write($msg);
        }
    }
    
    public function error($msg, $m1 = '', $m2 = '', $m3 = '', $m4 = '', $m5 = '', $m6 = '') 
    {
        if ($this->_logging()) {
            $msg = "error: " . print_r($msg, true) . print_r($m1, true) . print_r($m2, true) . print_r($m3, true) . print_r($m4, true) . print_r($m5, true) . print_r($m6, true);
            $this->_write($msg);
        }
        return false;
    }
    
    public function ex($ex, $msg)
    {
        if ($this->_logging()) {
            $msg = "an exception has occured during $msg: " . $ex->getMessage();
            $this->_write($msg);
        }
        return false;
    }
	
    public function debug($msg, $m1 = '', $m2 = '', $m3 = '', $m4 = '', $m5 = '', $m6 = '') 
    {
        if ($this->_logging(true)) {
            $msg = "debug: " . print_r($msg, true) . print_r($m1, true) . print_r($m2, true) . print_r($m3, true) . print_r($m4, true) . print_r($m5, true) . print_r($m6, true);
            $this->_write($msg);
        }
    }
    
    private function _logging($verbose = false)
    {
        return $verbose ? self::$debug : true;
    }
    
    private function _write($msg)
    {
        if (!self::$console) {
            date_default_timezone_set('UTC');
            $uri = $this->getLogDir() . self::$filename;
            self::$console = fopen($uri, "a");
        }
        
        if (self::$console) {
            $stats = fstat(self::$console);
            if ($stats) {
                $size = $stats['size'] / (1024*1000);
                if ($size > 10) {
                    @fclose(self::$console);
                    self::$console = @fopen($uri, "w");
                }
            }
        }
        
        if (self::$console) {
            $msg = strftime("%Y-%m-%d %H:%M:%S ") . " " . $msg;
            
            fwrite(self::$console, print_r($msg, true)."\n");
            fflush(self::$console);
        }
    }
    
}
