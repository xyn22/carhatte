<?php

require_once 'Ddd/Service/Model/StockService.php';
require_once 'Ddd/Service/Model/SaleService.php';

class Ddd_Service_Model_Service
{
   /*public function schedule(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('dddservice');
        $config = Mage::helper('dddservice')->getConfig();
        
        $stockImportPeriod = $config['cron']['stock_import_period'];
        if (!empty($stockImportPeriod) && !is_numeric($stockImportPeriod)) {
            $helper->error('Stock import period is not numeric');
            return;
        }
        $stockImportPeriod = (int)$stockImportPeriod;
        
        try {
            $xml = Mage::getConfig()->loadModulesConfiguration('config.xml')->getNode();
            $xml = file_get_contents('Ddd/Service/etc/config.xml', true);
            if (empty($xml)) {
                $helper->error('No config xml file');
                return;
            }
            $xml = simplexml_load_string($xml);
            if (empty($xml)) {
                $helper->error('No valid xml in config file');
                return;
            }
            
            $stockImportPeriodSchedule = $stockImportPeriod == 0 ? '' : $this->_toCronJob($stockImportPeriod);

            $xml->crontab->jobs->ddd_service_stock_import_task->schedule->cron_expr = $stockImportPeriodSchedule;

            $xml = $xml->asXml();
            if (file_put_contents('Ddd/Service/etc/config.xml', $xml, FILE_USE_INCLUDE_PATH) === false) {
                $helper->error('Unable to write to config.xml file');
            }
        } catch(Exception $ex) {
            $helper->ex($ex, 'Schedule');
            throw $ex;
        }
        Mage::app()->clearWebsiteCache();
    }*/

    public static function runStockImport()
    {
        $helper = Mage::helper('dddservice');
        $config = Mage::helper('dddservice')->getConfig();
        
        if (!$config['general']['enabled'] || !$config['general']['stock_import_enabled']) {
            $helper->debug('Stock import disabled');
            return;
        }
        $collection = Mage::getModel('catalog/product')->getCollection();

        foreach($collection as $product) {
            if (!$product || !$product->getId()) {
                continue;
            }
            $product = $product->load($product->getId());
            $ean = $product->getData('ean');
            if (empty($ean)) {
                $helper->warning('No ean for ', $product->getSku());
                continue;
            }


            $clientIds = Mage::getStoreConfig('settings/general/clientids');
            $idsArr = explode(",", trim($clientIds));


            if ( $idsArr )
            {
                foreach( $idsArr as $id )
                {
                    $clientId = trim($id);

                    try{
                        $qty = self::getStockCount($ean, $clientId);
                    }catch(Exception $e){
                        $helper->debug('Get QTY Error EAN=' . $ean);
                        continue;
                    }

                    $stockItem = $product->getStockItem();
                    if (!$stockItem || get_class($stockItem) == 'Varien_Object'){
                        $stockItem = Mage::getModel('cataloginventory/stock_item');
                        $stockItem->assignProduct($product)
                            ->setData('stock_id', 1);
                    }

                    if ( $clientId == '175099' )
                    {
                        $product->setKontor($qty);
                    }
                    else if ( $clientId == '175001' )
                    {
                        $product->setElmegade($qty);
                    }
                    else if ( $clientId == '175003' )
                    {
                        $product->setKrystalgade($qty);
                    }

                    $allQty = (int)$product->getKontor() + (int)$product->getElmegade() + (int)$product->getKrystalgade();

                    $product->setAllQty($allQty);
                    $product->save();

                    $stockItem->setData('qty', $allQty)
                        ->setData('is_in_stock', $allQty > 0 ? 1 : 0)
                        ->assignProduct($product)
                        ->setData('manage_stock', 1)
                        ->save();
                }
            }
        }

        return true;
    }

    private static function getStockCount($ean = '', $clientId = false){

        $count = 0;
        if (!$ean){
          return $count;
        }

        $request = new StockCountSingleEan();

        if ( $clientId )
        {
            $request->client = $clientId;
        }
        else
        {
            $request->client = Mage::getStoreConfig('settings/general/clientid');
        }

        $request->psk = Mage::getStoreConfig('settings/general/key');
        $request->ean = $ean;

        $service = new StockService();
        $response = $service->StockCountSingleEan($request);

        if (isset($response->StockCountSingleEanResult)) {
            if (isset($response->StockCountSingleEanResult->KeyValueOflongint)) {
                if (isset($response->StockCountSingleEanResult->KeyValueOflongint->Key) && $response->StockCountSingleEanResult->KeyValueOflongint->Key == $ean){
                   return (int)$response->StockCountSingleEanResult->KeyValueOflongint->Value;
                }
            }
        }

        return $count;

    }
    
    private static function _getStockLevel($eans)
    {
        $helper = Mage::helper('dddservice');
        $config = Mage::helper('dddservice')->getConfig();
        
        $request = new StockCount();
        $request->client = $config['general']['clientid'];
        $request->psk = $config['general']['key'];
        $request->eans = $eans;
        
        $service = new StockService();
        
        $helper->debug($request);
        $response = $service->StockCount($request);
        $helper->debug($response);
        
        $arr = array();
        if (isset($response->StockCountResult)) {
            if (isset($response->StockCountResult->KeyValueOflongint)) {
                foreach($response->StockCountResult->KeyValueOflongint as $entry) {
                    $arr[(string)$entry->Key] = (int)$entry->Value;
                }
            }
        }
        return $arr;
    }
    
    private function _toCronJob($minutes)
    {
        $hours = (int)($minutes/60);
        $minutes = $minutes % 60;
        return ($minutes > 0 ? "*/$minutes" : "0") . " " . ($hours > 0 ? "*/$hours" : "*") . " * * *";
    }

    public function reserve(Varien_Event_Observer $observer)
    {
      $order = $observer->getEvent()->getOrder();

      $service = new SaleService();
      $date = new DateTime();

      $sale = new Sale();
      $sale->Type = 'Sale';
      $sale->NoteID = $order->getIncrementId();
      $sale->Date = $date->format('Y-m-d\TH:i:s');
      $sale->ClientNumber = Mage::getStoreConfig('settings/general/clientid');
      $sale->Terminal = 99;

      $order_items = $order->getAllItems();
      $total_price = 0;

      foreach ($order_items as $order_item){

          $product = Mage::getModel('catalog/product')->load($order_item->getProductId());

          if($product->getTypeId() == 'simple') {
              $item = new ItemLine();
              $item->Type = 'Sale';
              $item->DateTime = $date->format('Y-m-d\TH:i:s');
              if ($product->getData('item_group')){
                $item->ItemGroup = $product->getData('item_group');
              }
              if ($product->getData('supplier')){
                $item->Supplier = $product->getData('supplier');
              }
              $item->Qty = $order_item->getQtyOrdered();
              $item->LineAmount = $product->getPrice();
              $item->EDBNumber = number_format($product->getData('ean'));
              $item->DiscountAmount = 0;
              $sale->ItemLines[] = $item;

              $total_price += $product->getPrice();
          }
      }

      $payment = new PaymentLine();
      $payment->Qty = 1;
      $payment->LineAmount = $total_price;
      $sale->PaymentLines[] = $payment;

      try{
        $result = $service->SaveSale($sale, Mage::getStoreConfig('settings/general/key'));
      }catch (Exception $e){
        
      }

    }

}