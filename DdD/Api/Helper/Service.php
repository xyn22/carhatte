<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api_Helper_Service
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Api_Helper_Service extends Mage_Core_Helper_Abstract
{

    protected $_wsdl;

    protected $_soapClient;

    protected $_clientId;

    protected $_apiKey;

    protected $_useCache = false;

    public function __construct($wsdl = null)
    {
        $this->_clientId = Mage::getStoreConfig('settings/general/clientid');
        $this->_apiKey = Mage::getStoreConfig('settings/general/key');
        if (null !== $wsdl) {
            $this->_wsdl = $wsdl;
        }
        if (!$this->_wsdl) {
            Mage::throwException('DdD wsdl not exists!');
        }
        $this->_soapClient = new SoapClient($this->_wsdl);
    }

    public function setClientId($clientId)
    {
        $this->_clientId = $clientId;
        return $this;
    }

    public function setApiKey($apiKey)
    {
        $this->_apiKey = $apiKey;
        return $this;
    }

    public function setUseCache($useCache)
    {
        $this->_useCache = (boolean) $useCache;
        return $this;
    }

    public function clearCache()
    {
        $iterator = new DirectoryIterator($this->_getCacheDir());
        foreach ($iterator as $item) {
            if ($item->isFile()) {
                unlink($this->_getCacheDir() . DIRECTORY_SEPARATOR . $item->getFilename());
            }
        }
        return $this;
    }

    public function getResult($method, $arguments = array(), array $options = array())
    {
        $result = null;
        if ($this->_useCache) {
            $prefix = isset($arguments['clientid']) ? $arguments['clientid'] : $arguments['client'];
            $cacheKey = $this->_createCacheKey(func_get_args(), $prefix);
            $result = $this->_getResultFromCache($cacheKey);
        }
        if (!$result) {
            Mage::log('Not use cache: ' . $cacheKey);
            $result = $this->_getApiResult($method, $arguments, $options);
        }
        if ($this->_useCache && $result) {
            $this->_setResultToCache($cacheKey, $result);
        }
        return $result;
    }

    public function __call($method, $arguments)
    {
        array_unshift($arguments, $method);
        return call_user_func_array(array($this, 'getResult'), $arguments);
    }

    protected function _getApiResult($method, $arguments = array(), array $options = array())
    {
        $resultName = $method . 'Result';
        if (isset($options['resultName'])) {
            $resultName = $options['resultName'];
            unset($options['resultName']);
        }
        ini_set('default_socket_timeout', 1200);
        $response = $this->_soapClient->__soapCall($method, array($arguments), $options);
        if (!$response) {
            Mage::throwException('The API does not have a response!');
        }
        return $this->_getResultFromResponse($resultName, $response);
    }

    protected function _getResultFromResponse($resultName, $response)
    {
        if (is_string($resultName) && isset($response->{$resultName})) {
            if (!is_string($response->{$resultName})) {
                $response->{$resultName} = json_encode($response->{$resultName});
            }
            return $response->{$resultName};
        }
        if (is_array($resultName)) {
            $resultNameString = array_shift($resultName);
            if (count($resultName) == 1) {
                $resultName = $resultName[0];
            }
            if (isset($response->{$resultNameString})) {
                $response = $response->{$resultNameString};
                return $this->_getResultFromResponse($resultName, $response);
            }
        }
    }

    protected function _getResultFromCache($cacheKey)
    {
        $cacheFile = $this->_getCacheFile($cacheKey);
        if (file_exists($cacheFile)) {
            return file_get_contents($cacheFile);
        }
    }

    protected function _setResultToCache($cacheKey, $result)
    {
        $cacheFile = $this->_getCacheFile($cacheKey);
        file_put_contents($cacheFile, $result);
        return $this;
    }

    protected function _createCacheKey($key, $prefix = '')
    {
        if (!is_string($key)) {
            $key = serialize($key);
        }
        return $prefix . md5($key);
    }

    protected function _getCacheFile($cacheKey)
    {
        $cacheKey = (string) $cacheKey;
        return $this->_getCacheDir() . DS . "{$cacheKey}.json";
    }

    protected function _getCacheDir()
    {
        $cacheDir = Mage::getBaseDir('media') . DS . 'ddd_api';
        if (!is_dir($cacheDir)) {
            mkdir($cacheDir, 0755, true);
        }
        return $cacheDir;
    }
}