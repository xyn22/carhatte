<?php

/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Api_Helper_Service
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */
class DdD_Api_Helper_Stock extends DdD_Api_Helper_Service
{

    protected $_wsdl = 'http://api.dddadmin.com/StockService.svc?wsdl';

    protected $_useJsonFormat = true;

    public function useJsonFormat($useJsonFormat = true)
    {
        $this->_useJsonFormat = $useJsonFormat;
        return $this;
    }

    public function getStockResult(array $arguments = array(), array $options = array())
    {
        $method = 'StockCount';
        $defaultArguments = array(
            'eans' => array(),
            'client' => (int) $this->_clientId,
            'psk' => $this->_apiKey
        );
        $arguments += $defaultArguments;
        $options = array(
            'resultName' => array('StockCountResult', 'KeyValueOflongint')
        );
        $result = $this->getResult($method, $arguments, $options);
        if (!$this->_useJsonFormat) {
            $result = json_decode($result);
        }
        return Mage::getModel('ddd_api/import_result_stock')->setData($result);
    }

    public function getProductResult(array $arguments = array(), array $options = array())
    {
        $method = 'GetWebshopArticles';
        $defaultArguments = array(
            'clientid' => (int) $this->_clientId,
            'psk' => $this->_apiKey
        );
        $arguments += $defaultArguments;
        $result = $this->getResult($method, $arguments, $options);
        if (!$this->_useJsonFormat) {
            $result = json_decode($result);
        }
        return Mage::getModel('ddd_api/import_result_product')->setData($result);
    }
}