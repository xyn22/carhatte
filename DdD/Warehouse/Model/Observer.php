<?php
class DdD_Warehouse_Model_Observer
{
    public function salesOrderItemUpdateQty(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();
        /* @var $orderItem Mage_Sales_Model_Order_Item */
        $orderItem = $event->getItem();
        if (!$orderItem->getQuoteParentItemId() && !$orderItem->getId()) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $orderItem->getSku());
            $qtyWarehouseOrdered = Mage::helper('ddd_warehouse')->getQtyWarehouseOrdered($product, $orderItem->getQtyOrdered());
            Mage::helper('ddd_warehouse/catalog_product')->updateQtyWarehouse($product, $qtyWarehouseOrdered);
            foreach ($qtyWarehouseOrdered as $warehouse => $qty) {
                if (isset($qty['qty_ordered']) && !empty($qty['qty_ordered'])) {
                    $fieldQtyWarehouse = 'qty_' . $warehouse;
                    $fieldQtyWarehouseOrdered = 'qty_' . $warehouse . '_ordered';
                    
                    $orderItem->setData($fieldQtyWarehouse, $qty['qty_warehouse']);
                    $orderItem->setData($fieldQtyWarehouseOrdered, $qty['qty_ordered']);
                }
            }
        }
    }

    public function salesOrderItemUpdateCancelQty(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();
        /* @var $orderItem Mage_Sales_Model_Order_Item */
        $orderItem = $event->getItem();
        if (!$orderItem->getParentItemId()) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $orderItem->getSku());
            $qtyWarehouseCanceled = Mage::helper('ddd_warehouse')->getQtyWarehouseCanceled($orderItem);
            Mage::helper('ddd_warehouse/catalog_product')->updateQtyWarehouse($product, $qtyWarehouseCanceled);
        }
    }

    public function salesOrderItemUpdateRefundQty(Varien_Event_Observer $observer)
    {
        /* @var $creditmemoItem Mage_Sales_Model_Order_Creditmemo_Item */
        $creditmemoItem = $observer->getEvent()->getCreditmemoItem();
        Mage::log('Enabled: ' . Mage::helper('cataloginventory')->isAutoReturnEnabled() . ', ' . $creditmemoItem->hasBackToStock());
        if (!Mage::helper('cataloginventory')->isAutoReturnEnabled() && !$creditmemoItem->hasBackToStock()) {
            return;
        }
        /* @var $orderItem Mage_Sales_Model_Order_Item */
        $orderItem = $creditmemoItem->getOrderItem();
        if (!$orderItem->getParentItemId()) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $orderItem->getSku());
            $qtyWarehouseRefunded = Mage::helper('ddd_warehouse')->getQtyWarehouseRefunded($orderItem, $creditmemoItem->getQty());
            Mage::log('Creditmemo Qty: ' . $creditmemoItem->getQty());
            Mage::log('Refund Qty: ' . json_encode($qtyWarehouseRefunded));
            Mage::helper('ddd_warehouse/catalog_product')->updateQtyWarehouse($product, $qtyWarehouseRefunded);
        }
    }
}