<?php
/* @var $installer MageBetter_Sales_Model_Resource_Setup */
$installer = $this;
$tableName = $installer->getTable('sales/order_item');
$fields = array(
    'qty_elmegade' => array(
        'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Qty Elmegade',
        'scale'     => 4,
        'precision' => 12
        
    ),
    'qty_elmegade_ordered' => array(
        'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Qty Elmegade Ordered',
        'scale'     => 4,
        'precision' => 12
        
    ),
    'qty_krystalgade' => array(
        'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Qty Krystalgade',
        'scale'     => 4,
        'precision' => 12
    ),
    'qty_krystalgade_ordered' => array(
        'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Qty Krystalgade Oredered',
        'scale'     => 4,
        'precision' => 12
    )
);
foreach ($fields as $fieldName => $options) {
    $installer->getConnection()
        ->addColumn($tableName, $fieldName, $options);
}
$installer->endSetup();
