<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Warehouse_Block_Adminhtml_Dashboard_Tab_Products_Ordered
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Warehouse_Block_Adminhtml_Dashboard_Tab_Products_Ordered extends Mage_Adminhtml_Block_Sales_Order_View_Items
{
    public function getItemsCollection()
    {
        /* @var $collection Mage_Sales_Model_Resource_Order_Item_Collection */
        $collection = Mage::getModel('sales/order_item')->getCollection()
            ->setOrder('created_at', 'desc');
        $this->_prepareCollection($collection);
        return $collection;
    }

    public function getOrder()
    {
    }

    public function canEditQty()
    {
        return false;
    }

    protected function _prepareCollection(Mage_Sales_Model_Resource_Order_Item_Collection $collection)
    {
        $warehouse = $this->_getWarehouse();
        $collection->addFieldToFilter('qty_' . $warehouse . '_ordered', array('notnull' => true))
            ->addFieldToFilter('created_at', array(
                'from' => date('Y-m-d 00:00:00', strtotime('last day')),
                'to' => date('Y-m-d 23:59:59', strtotime('today')),
            ));
    }

    protected function _getWarehouse()
    {
        return Mage::app()->getRequest()->getParam('warehouse');
    }
}