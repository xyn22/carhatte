<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Warehouse_Block_Adminhtml_Warehouse_Sales_Order_Items_Grid_Column_Renderer_Default
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Warehouse_Block_Adminhtml_Warehouse_Sales_Order_Items_Grid_Column_Renderer_Default
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text
{
    public function render(Varien_Object $row)
    {
        $block = $this->_getBlockColumn();
        $block->setItem($row);
        $block->setPriceDataObject($row);
        $priceAttribute = $this->getColumn()->getRendererPriceAttribute();
        if ($priceAttribute) {
            return $block->displayPriceAttribute($priceAttribute);
        }
        return $block->toHtml();
    }

    /**
     * @return Mage_Adminhtml_Block_Sales_Order_View_Items_Renderer_Default
     */
    protected function _getBlockColumn()
    {
        $block = $this->getLayout()->createBlock('adminhtml/sales_order_view_items_renderer_default');
        $template = $this->getColumn()->getRendererTemplate();
        if ($template) {
            $block->setTemplate($template);
        }
        return $block;
    }
}