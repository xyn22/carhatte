<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Warehouse_Block_Adminhtml_Warehouse_Sales_Order_Items_Grid
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Warehouse_Block_Adminhtml_Warehouse_Sales_Order_Items_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected function _getCollectionClass()
    {
        return 'ddd_warehouse/sales_order_item_collection';
    }

    protected function _prepareCollection()
    {
        /* @var $collection DdD_Warehouse_Model_Resource_Sales_Order_Item_Collection */
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $collection->getSelect()->joinLeft(
            array('order_table' => $collection->getTable('sales/order')),
            'main_table.order_id = order_table.entity_id'
        );
        $collection->addFieldToFilter('parent_item_id', array('null' => true))
            ->addFieldToFilter('qty_invoiced', array('eq' => 0))
            ->addFieldToFilter('order_table.state', array('eq' => Mage_Sales_Model_Order::STATE_PROCESSING))
            ->setOrder('item_id', Varien_Db_Select::SQL_DESC);
        $collection->getSelect()->where('qty_canceled < qty_ordered');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
            $field = ( $column->getFilterIndex() ) ? $column->getFilterIndex() : $column->getIndex();
            if ($column->getFilterConditionCallback()) {
                call_user_func($column->getFilterConditionCallback(), $this->getCollection(), $column);
            } else {
                $cond = $column->getFilter()->getCondition();
                $filterField = $column->getFilter()->getField();
                if ($filterField) {
                    $field = $filterField;
                }
                if ($field && isset($cond)) {
                    $this->getCollection()->addFieldToFilter($field , $cond);
                }
            }
        }
        return $this;
    }

    protected function _prepareColumns()
    {
//         $this->addColumn('item_id', array(
//             'header' => Mage::helper('sales')->__('ID'),
//             'align' => 'center',
//             'index' => 'item_id',
//             'width' => '25px'
//         ));
        $this->addColumn('name', array(
            'header' => Mage::helper('sales')->__('Product'),
            'align' => 'left',
            'index' => 'name',
            'filter_condition_callback' => array($this, '_addNameToFilterCondition'),
            'renderer' => 'ddd_warehouse/adminhtml_warehouse_sales_order_items_grid_column_renderer_name'
        ));
        $this->addColumn('warehouse', array(
            'header' => $this->helper('ddd_warehouse')->__('Warehouse'),
            'align' => 'right',
            'width' => '110px',
            'filter' => 'ddd_warehouse/adminhtml_warehouse_sales_order_items_grid_column_filter_warehouse',
            'renderer' => 'ddd_warehouse/adminhtml_warehouse_sales_order_items_grid_column_renderer_warehouse',
            'sortable' => false,
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Item Status'),
            'align' => 'right',
            'width' => '70px',
            'filter' => false,
            'renderer' => 'ddd_warehouse/adminhtml_warehouse_sales_order_items_grid_column_renderer_status',
            'sortable' => false
        ));
//         $this->addColumn('original_price', array(
//             'header' => Mage::helper('sales')->__('Orginal Price'),
//             'align' => 'right',
//             'width' => '70px',
//             'index' => 'original_price',
//             'type' => 'range',
//             'renderer' => 'ddd_warehouse/adminhtml_warehouse_sales_order_items_grid_column_renderer_default',
//             'renderer_price_attribute' => 'original_price'
//         ));
//         $this->addColumn('price', array(
//             'header' => Mage::helper('sales')->__('Price'),
//             'align' => 'right',
//             'width' => '70px',
//             'index' => 'price',
//             'type' => 'range',
//             'renderer' => 'ddd_warehouse/adminhtml_warehouse_sales_order_items_grid_column_renderer_default',
//             'renderer_template' => 'ddd/sales/items/column/price.phtml'
//         ));
        $this->addColumn('qty', array(
            'header' => Mage::helper('sales')->__('Qty'),
            'align' => 'center',
            'width' => '70px',
            'filter' => false,
            'renderer' => 'ddd_warehouse/adminhtml_warehouse_sales_order_items_grid_column_renderer_qty',
            'sortable' => false
        ));
//         $this->addColumn('sub_total', array(
//             'header' => Mage::helper('sales')->__('Subtotal'),
//             'align' => 'right',
//             'width' => '70px',
//             'index' => 'row_total',
//             'filter' => false,
//             'renderer' => 'ddd_warehouse/adminhtml_warehouse_sales_order_items_grid_column_renderer_default',
//             'renderer_template' => 'ddd/sales/items/column/subtotal.phtml'
//         ));
//         $this->addColumn('tax_amount', array(
//             'header' => Mage::helper('sales')->__('Tax Amount'),
//             'align' => 'right',
//             'width' => '70px',
//             'index' => 'tax_amount',
//             'type' => 'range',
//             'renderer' => 'ddd_warehouse/adminhtml_warehouse_sales_order_items_grid_column_renderer_default',
//             'renderer_price_attribute' => 'tax_amount'
//         ));
//         $this->addColumn('tax_percent', array(
//             'header' => Mage::helper('sales')->__('Tax Percent'),
//             'align' => 'right',
//             'width' => '70px',
//             'index' => 'tax_percent',
//             'filter' => false,
//             'renderer' => 'ddd_warehouse/adminhtml_warehouse_sales_order_items_grid_column_renderer_default',
//             'renderer_template' => 'ddd/sales/items/column/taxpercent.phtml'
//         ));
//         $this->addColumn('discount_amount', array(
//             'header' => Mage::helper('sales')->__('Discount Amount'),
//             'align' => 'right',
//             'width' => '70px',
//             'index' => 'discount_amount',
//             'type' => 'range',
//             'renderer' => 'ddd_warehouse/adminhtml_warehouse_sales_order_items_grid_column_renderer_default',
//             'renderer_price_attribute' => 'discount_amount'
//         ));
//         $this->addColumn('row_total', array(
//             'header' => Mage::helper('sales')->__('Row Total'),
//             'align' => 'right',
//             'width' => '70px',
//             'index' => 'row_total',
//             'type' => 'range',
//             'renderer' => 'ddd_warehouse/adminhtml_warehouse_sales_order_items_grid_column_renderer_default',
//             'renderer_template' => 'ddd/sales/items/column/rowtotal.phtml'
//         ));
        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Created At'),
            'index' => 'created_at',
            'filter_index' => 'main_table.created_at',
            'type' => 'datetime',
            'filter_time' => true,
            'width' => '120px',
        ));
    }

    protected function _addNameToFilterCondition($collection, $column)
    {
        $filterValue = $column->getFilter()->getValue();
        if ($filterValue) {
            $condition = array('like' => "%{$filterValue}%");
            $collection->addFieldToFilter(array('name', 'sku'), array($condition, $condition));
        }
    }
}