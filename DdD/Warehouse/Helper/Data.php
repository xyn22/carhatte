<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2014 OlyThy (olythy@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   DdD
 * @package    DdD_Warehouse_Helper_Data
 * @copyright  Copyright (c) 2014 OlyThy (olythy@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

class DdD_Warehouse_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_warehouses = array(
        175001 => 'elmegade',
        175003 => 'krystalgade'
        //175099 => 'kontor',
    );

    protected $_mainWarehouseId = 175001;

    public function checkWarehouseExists($warehouse)
    {
        return in_array($warehouse, $this->_warehouses);
    }

    public function getWarehouses()
    {
        return $this->_warehouses;
    }

    public function getWarehouseById($id)
    {
        if (!isset($this->_warehouses[$id])) {
            Mage::throwException('Warehouse does not exists for ID "' . $id . '"');
        }
        return $this->_warehouses[$id];
    }

    public function getMainWarehouse()
    {
        return $this->getWarehouseById($this->_mainWarehouseId);
    }

    public function getQtyWarehouse(Mage_Catalog_Model_Product $product)
    {
        $qtyWarehouse = array();
        foreach ($this->_warehouses as $warehouse) {
            if ($product->hasData($warehouse)) {
                $qtyWarehouse[$warehouse] = $product->getData($warehouse);
            }
        }
        return $qtyWarehouse;
    }

    public function getQtyWarehouseOrdered(Mage_Catalog_Model_Product $product, $qtyOrdered)
    {
        $qtyWarehouseOrdered = array();
        $qtyWarehouse = $this->getQtyWarehouse($product);
        foreach($qtyWarehouse as $warehouse => $qty) {
            if ($qty >= $qtyOrdered) {
                $qtyWarehouseOrdered[$warehouse] = array('qty_ordered' => $qtyOrdered, 'qty_warehouse' => $qty);
                break;
            } else {
                $qtyOrdered = $qtyOrdered - $qty;
                $qtyWarehouseOrdered[$warehouse] = array('qty_ordered' => $qty, 'qty_warehouse' => $qty);;
            }
        }
        return $qtyWarehouseOrdered;
    }

    public function getQtyWarehouseCanceled(Mage_Sales_Model_Order_Item $orderItem)
    {
        $qtyWarehouseCanceled = array();
        $qtyCanceled = $orderItem->getQtyToCancel();
        foreach ($this->_warehouses as $warehouse) {
            $qtyWarehouseOrderedField = 'qty_' . $warehouse . '_ordered';
            $qtyWarehouseOrdered = $orderItem->getData($qtyWarehouseOrderedField);
            if ($qtyWarehouseOrdered && $qtyCanceled <= $qtyWarehouseOrdered) {
                $qtyWarehouseCanceled[$warehouse] = array('qty_canceled' => $qtyCanceled);
                $orderItem->setData($qtyWarehouseOrderedField, ($qtyWarehouseOrdered - $qtyCanceled));
                break;
            } elseif ($qtyWarehouseOrdered && $qtyCanceled > $qtyWarehouseOrdered) {
                $qtyCanceled = $qtyCanceled - $qtyWarehouseOrdered;
                $qtyWarehouseCanceled[$warehouse] = array('qty_canceled' => $qtyWarehouseOrdered);
                $orderItem->setData($qtyWarehouseOrderedField, 0);
            }
        }
        return $qtyWarehouseCanceled;
    }

    public function getQtyWarehouseRefunded(Mage_Sales_Model_Order_Item $orderItem, $qtyRefunded)
    {
        $qtyWarehouseRefunded[$this->getMainWarehouse()] = array(
            'qty_refunded' => $qtyRefunded
        );
        $warehouses = array_reverse($this->_warehouses);
        foreach ($warehouses as $warehouse) {
            $qtyWarehouseOrderedField = 'qty_' . $warehouse . '_ordered';
            $qtyWarehouseOrdered = $orderItem->getData($qtyWarehouseOrderedField);
            if ($qtyWarehouseOrdered && $qtyRefunded <= $qtyWarehouseOrdered) {
                $orderItem->setData($qtyWarehouseOrderedField, ($qtyWarehouseOrdered - $qtyRefunded));
                break;
            } elseif ($qtyWarehouseOrdered && $qtyRefunded > $qtyWarehouseOrdered) {
                $qtyRefunded = $qtyRefunded - $qtyWarehouseOrdered;
                $orderItem->setData($qtyWarehouseOrderedField, 0);
            }
        }
        return $qtyWarehouseRefunded;
    }

    public function getAdminhtmlHelper()
    {
        return Mage::helper('ddd_warehouse/adminhtml');
    }
}
